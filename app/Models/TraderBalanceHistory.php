<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class TraderBalanceHistory extends Model
{
    use HasFactory;

    const TYPE_CREDIT = 0;
    const TYPE_DEBIT = 1;

    protected $fillable = [
        'amount',
        'balance',
        'balance_id',
        'balance_type',
        'currency',
        'currency_id',
        'description',
        'invoice_id',
        'invoice_type',
        'status',
        'trader_id',
        'type',
    ];

}

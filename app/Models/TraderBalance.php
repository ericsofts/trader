<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TraderBalance extends Model
{
    use HasFactory;

    const TYPE_NORMAL = 0;
    const TYPE_FROZEN = 1;

    protected $fillable = [
        'trader_id', 'amount', 'currency', 'currency_id', 'type'
    ];

    public function getType(){
        return [
            self::TYPE_NORMAL => 'Normal',
            self::TYPE_FROZEN => 'Frozen',
        ];
    }
}

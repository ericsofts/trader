<?php

namespace App\Models;

use App\Models\PaymentSystemType;
use App\Models\TraderPaymentSystem;
use App\Models\TraderFiat;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class TraderPaymentAccount extends Model
{
    use HasFactory;
    use SoftDeletes;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    protected $fillable = [
        'payment_system_id', 'payment_system_type_id', 'trader_fiat_id', 'card_number', 'status', 'limit_amount', 'volume', 'name', 'last_paid_at', 'last_selected_at', 'payment_info'
    ];

    protected $dates = ['deleted_at'];

    public function paySystem()
    {
        return $this->hasOne(TraderPaymentSystem::class, 'id', 'payment_system_id');
    }

    public function fiat()
    {
        return $this->hasOne(TraderFiat::class, 'id', 'trader_fiat_id');
    }

    public function payType()
    {
        return $this->hasOne(PaymentSystemType::class, 'id', 'payment_system_type_id');
    }

    public static function availableAccounts($payment_system_id, $payment_system_type_id, $trader_ad_id = null)
    {
        $result = TraderPaymentAccount::where([
            'trader_id' => Auth::user()->id,
            'payment_system_id' => $payment_system_id,
            'payment_system_type_id' => $payment_system_type_id,
            'status' => self::STATUS_ACTIVE
        ])->get();

        if ($trader_ad_id) {
            $adAccounts = TraderAdAccountRelation::where('trader_ad_id', $trader_ad_id)->get();

            $account_ids = [];
            foreach ($adAccounts as $unit) {
                $account_ids[] = $unit->trader_payment_account_id;
            }

            foreach ($result as &$unit) {
                if (in_array($unit->id, $account_ids)) {
                    $unit->checked = true;
                }
            }
        }

        return $result;
    }
}

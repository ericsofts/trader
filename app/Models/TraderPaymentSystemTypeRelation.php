<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TraderPaymentSystemTypeRelation extends Model
{
    use HasFactory;
    protected $fillable = [
        'trader_payment_system_id', 'payment_system_type_id'
    ];
}

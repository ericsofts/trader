<?php

namespace App\Models;

use App\Traits\Encrypt;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Trader extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Encrypt;

    const STATUS_BLOCKED = 0;

    protected $encrypts = ['secret_2fa'];

    protected $fillable = [
        'name', 'email', 'password', 'status', 'email_verified_at', 'remember_token', 'payment_info', 'trader', 'timezone', 'default_currency'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function trader_deal_status_codes()
    {
        return $this->hasMany(TraderDealStatusCode::class, 'trader_id', 'id');
    }

    public function getTotalBalance($currency_id = 1, $balanceType = TraderBalance::TYPE_NORMAL){
        $currency = TraderCurrency::find($currency_id);

        $balance = TraderBalance::firstOrCreate([
            'trader_id' => $this->id,
            'type' => $balanceType,
            'currency_id' => $currency_id,
            'currency' => $currency->asset
        ]);

        $balanceQueueCredit = TraderBalanceQueue::where([
            'balance_id' => $balance->id,
            'type' => TraderBalanceQueue::TYPE_DEBIT
        ])
            ->whereNotIn('status', [TraderBalanceQueue::STATUS_DONE, TraderBalanceQueue::STATUS_FAILED])
            ->sum('amount');

        return round($balance->amount - $balanceQueueCredit, 8);
    }

    public function payment_accounts()
    {
        return $this->hasMany(TraderPaymentAccount::class, 'trader_id', 'id')->orderBy('created_at', 'desc');
    }
}

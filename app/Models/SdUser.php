<?php

namespace App\Models;

use App\Permissions\HasPermissionsTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class SdUser extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    const STATUS_BLOCKED = 0;

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $fillable = [
        'name',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TraderAd extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const TYPE_BUY = 1;
    const TYPE_SELL = 0;
    const RATE_FIXED = 0;
    const RATE_PERCENTAGE = 1;

    const ACCOUNT_ORDER_RANDOM = 0;
    const ACCOUNT_ORDER_CICLICALLY = 1;

    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'trader_id', 'type', 'fiat_id', 'currency_id', 'payment_system_id', 'card_number', 'min_amount', 'max_amount', 'rate_type', 'rate', 'status',
        'rate_source_id', 'rate_stop', 'payment_system_type_id', 'payment_info', 'auto_acceptance', 'account_order'
    ];

    public static function getTypes(): array
    {
        return [
            self::TYPE_BUY => 'Buy',
            self::TYPE_SELL => 'Sell',
        ];
    }

    public static function getRouteTypes(): array
    {
        return [
            self::TYPE_BUY => 'buy',
            self::TYPE_SELL => 'sell',
        ];
    }

    public static function getStatuses(): array
    {
        return [
            self::STATUS_ACTIVE => 'Enable',
            0 => 'Disable',
        ];
    }

    public static function getRates(): array
    {
        return [
            self::RATE_FIXED => 'Fixed',
            self::RATE_PERCENTAGE => 'Percentage',
        ];
    }

    public function fiat()
    {
        return $this->hasOne(TraderFiat::class, 'id', 'fiat_id');
    }

    public function payment_system()
    {
        return $this->hasOne(TraderPaymentSystem::class, 'id', 'payment_system_id');
    }

    public function currency()
    {
        return $this->hasOne(TraderCurrency::class, 'id', 'currency_id');
    }

    public function payment_system_type()
    {
        return $this->hasOne(PaymentSystemType::class, 'id', 'payment_system_type_id');
    }

    public function payment_accounts()
    {
        return $this->belongsToMany(TraderPaymentAccount::class, 'trader_ad_account_relations',  'trader_ad_id', 'trader_payment_account_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TraderCurrencyAddress extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'trader_id', 'currency_id', 'address '
    ];

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class PaymentSystemType extends Model
{
    use HasFactory;

    protected $fillable = [
        'obj_name', 'name', 'payment_info_name'
    ];

}

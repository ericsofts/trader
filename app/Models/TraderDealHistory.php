<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class TraderDealHistory extends Model
{
    use HasFactory;

    protected $fillable = [
        'deal_id',
        'description',
        'status'
    ];

    public function deal() {
        return $this->hasOne(TraderDeal::class, 'id', 'deal_id');
    }

}

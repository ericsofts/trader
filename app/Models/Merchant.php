<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Merchant extends Model
{
    use HasFactory;

    const STATUS_BLOCKED = 0;

    protected $fillable = [
        'name', 'email', 'password', 'token', 'status', 'payment_key', 'api3_key', 'type', 'agent_id', 'api5_key', 'enable_2fa', 'email_verified_at'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

}

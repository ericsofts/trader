<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TraderPaymentSystem extends Model
{
    use HasFactory;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    protected $fillable = [
        'name', 'status', 'color', 'bg_color'
    ];

    public function fiats()
    {
        return $this->belongsToMany(TraderFiat::class, 'trader_fiats_payment_systems', 'payment_system_id', 'fiat_id');
    }

    public function available_payment_types()
    {
        return $this->belongsToMany(PaymentSystemType::class, 'trader_payment_system_type_relations', 'trader_payment_system_id', 'payment_system_type_id');
    }
}

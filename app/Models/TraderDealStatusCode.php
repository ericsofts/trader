<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TraderDealStatusCode extends Model
{
    use HasFactory;

    protected $fillable = [
        'trader_id', 'status', 'code'
    ];

    public static function getAvalableStatus(){
        return [
            'STATUS_TRADER_FUNDED' => __('status_2'),
            'STATUS_CANCELED_TRADER' => __('status_97'),
            'STATUS_COMPLETED' => __('status_1'),
            'STATUS_DISPUTED' => __('status_5'),
            'STATUS_PAYMENT_COMPLETED_TRADER' => __('status_4'),
        ];
    }

    public static function getAvalableStatusCode(){
        return [
            'STATUS_TRADER_FUNDED' => TraderDeal::STATUS_TRADER_FUNDED,
            'STATUS_CANCELED_TRADER' => TraderDeal::STATUS_CANCELED_TRADER,
            'STATUS_COMPLETED' => TraderDeal::STATUS_COMPLETED,
            'STATUS_DISPUTED' => TraderDeal::STATUS_DISPUTED,
            'STATUS_PAYMENT_COMPLETED_TRADER' => TraderDeal::STATUS_PAYMENT_COMPLETED_TRADER,
        ];
    }

    public static function getAvalableStatusData(){
        $avalableStatuses = self::getAvalableStatus();
        $avalableStatusCode = self::getAvalableStatusCode();
        $result = [];
        foreach ($avalableStatuses as $key=>$avalableStatus){
            $result[$key] = [
                'label' => $avalableStatus,
                'code' => $avalableStatusCode[$key]
            ];
        }
        return $result;
    }
}

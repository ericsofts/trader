<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TraderDeal extends Model
{
    const TYPE_BUY = 1;
    const TYPE_SELL = 0;

    const STATUS_CREATE = 0;
    const STATUS_COMPLETED = 1;
    const STATUS_TRADER_FUNDED = 2;
    const STATUS_PAYMENT_COMPLETED_USER = 3;
    const STATUS_PAYMENT_COMPLETED_TRADER = 4;
    const STATUS_DISPUTED = 5;
    const STATUS_CANCELED_BY_REPLY_TIMEOUT = 94;
    const STATUS_CANCELED_TIMEOUT = 95;
    const STATUS_CANCELED_SD = 96;
    const STATUS_CANCELED_TRADER = 97;
    const STATUS_CANCELED_USER = 98;
    const STATUS_CANCELED = 99;

    const MERCHANT_USER = 0;
    const SD_USER = 1;
    const TRADER_USER = 2;
    const PS_USER = 3;
    const SYSTEM = 4;

    use HasFactory;

    protected $fillable = [
        'ad_id', 'trader_id', 'type', 'fiat_id', 'currency_id', 'payment_system_id', 'card_number', 'amount_currency', 'amount_fiat', 'rate', 'status', 'ad_json', 'payment_info', 'payment_account_id'
    ];

    public static function getTypes(): array
    {
        return [
            self::TYPE_BUY => 'Buy',
            self::TYPE_SELL => 'Sell',
        ];
    }

    public static function getRouteTypes(): array
    {
        return [
            self::TYPE_BUY => 'buy',
            self::TYPE_SELL => 'sell',
        ];
    }

    public static function allowedToChangeStatus() {
        return [
            self::TYPE_BUY => [
                self::STATUS_CREATE => [
                    self::STATUS_TRADER_FUNDED,
                    self::STATUS_CANCELED_TRADER
                ],
                self::STATUS_TRADER_FUNDED => [
                    self::STATUS_PAYMENT_COMPLETED_TRADER,
                    self::STATUS_CANCELED_TRADER
                ],
                self::STATUS_PAYMENT_COMPLETED_TRADER => [
                    self::STATUS_DISPUTED
                ]
            ],
            self::TYPE_SELL => [
                self::STATUS_CREATE => [
                    self::STATUS_TRADER_FUNDED,
                    self::STATUS_CANCELED_TRADER
                ],
                self::STATUS_PAYMENT_COMPLETED_USER => [
                    self::STATUS_COMPLETED,
                    self::STATUS_DISPUTED
                ],
                self::STATUS_DISPUTED => [
                    self::STATUS_COMPLETED
                ]
            ]
        ];
    }

    public function payment_system()
    {
        return $this->hasOne(TraderPaymentSystem::class, 'id', 'payment_system_id');
    }

    public function fiat()
    {
        return $this->hasOne(TraderFiat::class, 'id', 'fiat_id');
    }

    public function currency()
    {
        return $this->hasOne(TraderCurrency::class, 'id', 'currency_id');
    }

    public function statusHistory(){
        return $this->hasOne(TraderDealHistory::class, 'id', 'last_history_id');
    }

    public function ad()
    {
        return $this->hasOne(TraderAd::class, 'id', 'ad_id');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TraderAdAccountRelation extends Model
{
    protected $fillable = [
        'trader_ad_id', 'trader_payment_account_id'
    ];
}

<?php
namespace App\Http\Libs\Api;

use Illuminate\Support\Facades\Storage;

class Xpubd extends Base
{
    public function __construct($token = null)
    {
        if (is_null($token))
            $token = config('app.crypto_gate_token');
        parent::__construct($token);
    }

    public function addrNew($params = [])
    {
        $response = $this->httpClient->post(
            config('app.crypto_gate_host'),
            $this->prepare_params('address.new', $params)
        );

        if ($response->ok()) {
            $result = $response->json();

            if (!empty($result[0]['result'])) {
                return $result[0]['result'];
            }
        } else {
            return false;
        }
        return false;
    }

}
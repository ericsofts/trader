<?php

namespace App\Http\Controllers;

use App\Models\TraderAd;
use App\Models\TraderCurrency;
use App\Models\TraderDealHistory;
use App\Models\TraderDealMessage;
use App\Models\TraderDealStatusCode;
use App\Models\TraderFiat;
use App\Models\TraderPaymentAccount;
use App\Models\TraderPaymentSystem;
use App\Models\PaymentSystemType;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\TraderDeal;
use App\Http\Libs\Api\Trader as TraderClient;

class TraderDealController extends Controller
{

    private $traderClient;

    public function __construct(
        TraderClient $traderClient
    ) {
        $this->traderClient = $traderClient;
    }

    public function index(Request $request, $type)
    {
        $types = TraderAd::getTypes();
        $route_types = TraderAd::getRouteTypes();

        $typeId = array_flip($route_types)[$type];

        $limit = config('app.default.pagination_limit');
        $from_date = $request->input('from_date', null);
        $to_date = $request->input('to_date', null);
        $currencyId = $request->input('currency_id', null);
        $fiatId = $request->input('fiat_id', null);
        $status = $request->input('status', null);
        $paymentSystemId = $request->input('payment_system_id', null);
        $dealId = $request->input('deal_id', null);

        $messages = [];
        $rules = [
            'currency_id' => 'nullable|numeric',
            'fiat_id' => 'nullable|numeric',
            'status' => 'nullable|numeric',
            'payment_system_id' => 'nullable|numeric',
            'type' => 'nullable|numeric',
            'deal_id' => 'nullable|numeric',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        $query = TraderDeal::where(['trader_id' => Auth::user()->id]);
        if (!empty($from_date)) {
            try {
                $from_date = Carbon::parse($from_date, Auth::user()->timezone ?? config('app.default.timezone'));
                $from_date = $from_date->timezone('UTC');
            } catch (Exception $e) {
                $from_date = null;
            }
        }
        if (!empty($to_date)) {
            try {
                $to_date = Carbon::parse($to_date, Auth::user()->timezone ?? config('app.default.timezone'));
                $to_date = $to_date->timezone('UTC');
            } catch (Exception $e) {
                $to_date = null;
            }
        }
        if ($from_date) {
            $query->where('created_at', '>=', $from_date);
        }
        if ($to_date) {
            $query->where('created_at', '<=', $to_date);
        }
        if (!is_null($status)) {
            $query->where('status', '=', $status);
        }
        if (!is_null($currencyId)) {
            $query->where('currency_id', '=', $currencyId);
        }
        if (!is_null($fiatId)) {
            $query->where('fiat_id', '=', $fiatId);
        }
        if (!is_null($paymentSystemId)) {
            $query->where('payment_system_id', '=', $paymentSystemId);
        }
        if (!is_null($typeId)) {
            $query->where('type', '=', $typeId);
        }

        if (!is_null($dealId)) {
            $query->where('id', '=', $dealId);
        }

        $query->with(['fiat', 'payment_system', 'currency']);
        $query->orderBy('id', 'desc');
        $trader_deals = $query->paginate($limit)->withQueryString();

        $statuses = $this->traderClient->getTraderDealStatus();
        $currencies = TraderCurrency::all()->pluck('name', 'id');
        $fiats = TraderFiat::all()->pluck('name', 'id');
        $paymentSystems = TraderPaymentSystem::all()->pluck('name', 'id');

        return view('trader_deal.index', [
            'from_date' => $from_date ? $from_date->timezone(Auth::user()->timezone ?? config('app.default.timezone')) : null,
            'to_date' => $to_date ? $to_date->timezone(Auth::user()->timezone ?? config('app.default.timezone')) : null,
            'currency' => $currencyId,
            'fiat' => $fiatId,
            'status' => $status,
            'paymentSystem' => $paymentSystemId,
            'statuses' => $statuses,
            'type' => $type,
            'types' => $types,
            'route_types' => $route_types,
            'currencies' => $currencies,
            'fiats' => $fiats,
            'paymentSystems' => $paymentSystems,
            'trader_deals' => $trader_deals,
            'typeId' => $typeId,
            'dealId' => $dealId
        ]);
    }

    public function history(Request $request, $id)
    {
        $limit = config('app.default.pagination_limit');
        $route_types = TraderAd::getRouteTypes();

        $deal = TraderDeal::where(['id' => $id, 'trader_id' => $request->user()->id])->firstOrFail();

        $traderId = $deal->trader_id ?? null;
        if ($traderId != Auth::user()->id) {
            return redirect()->route('trader_deal.index')->with(['alert-type' => 'error', 'message' => __('Error')]);
        }

        $dealHistoryQuery = TraderDealHistory::where('deal_id', '=', $id)->orderBy('created_at', 'desc');
        $statuses = $this->traderClient->getTraderDealStatus();
        $statusesAvalable = $this->traderClient->getTraderDealAvalableStatus();
        $customerType = $this->traderClient->getCustomerType();
        $status = $dealHistoryQuery->first()->status ?? null;
        $dealHistory = $dealHistoryQuery->paginate($limit)->withQueryString();

        foreach ($dealHistory as $history) {
            if ($history->customer_type == TraderDeal::SYSTEM) {
                $history->customer_name = $customerType[TraderDeal::SYSTEM]['label'];
            } elseif ($history->customer_type) {
                $instance = app($customerType[$history->customer_type]['instance']);
                $history->customer_name = $instance::where('id', '=', $history->customer_id)->first()->name ?? '';
            }
        }
        return view('trader_deal.history', [
            'route_types' => $route_types,
            'deal' => $deal,
            'dealHistory' => $dealHistory,
            'statuses' => $statuses,
            'status' => $status,
            'customerType' => $customerType,
            'statusesAvalable' => $statusesAvalable[$status][TraderDeal::TRADER_USER] ?? [],
            'id' => $id
        ]);
    }

    public function status(Request $request, $id)
    {
        $description = $request->input('description', null);
        $status = $request->input('status', null);
        $deal = TraderDeal::where(['id' => (int) $id, 'trader_id' => (int) $request->user()->id])->firstOrFail();

        $allowedStatuses = TraderDeal::allowedToChangeStatus()[$deal->type][$deal->status] ?? [];
        if (!in_array($status, $allowedStatuses)) {
            return redirect()->back()->with(['alert-type' => 'error', 'message' => __('The Deal Status Change Error')]);
        }

        $messages = [];
        $rules = [
            'description' => 'nullable|string',
            'status' => 'required|numeric',
        ];
        $statusCode = TraderDealStatusCode::where([
            'trader_id' => $request->user()->id,
            'status' => $status
        ])->first();

        if ($statusCode && $statusCode->code) {
            $rules['code'] = ['required', 'string', function ($attribute, $value, $fail) use ($statusCode) {
                if ($statusCode->code != $value) {
                    $fail(__('The code you provided is not valid.'));
                }
            }];
        }

        if ($status == TraderDeal::STATUS_PAYMENT_COMPLETED_TRADER) {
            $rules['document'] = 'nullable|image|mimes:jpeg,png,jpg,gif';
        }

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            if (!empty($validator->errors()->messages()['code'][0])) {
                return redirect()->back()->with(['alert-type' => 'error', 'message' => $validator->errors()->messages()['code'][0]]);
            }

            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        if ($status == TraderDeal::STATUS_PAYMENT_COMPLETED_TRADER) {
            if ($request->hasFile('document')) {
                $file = $request->file('document');

                if ($file) {
                    $filePath = $this->traderClient->saveFile($file, $file->getClientOriginalName());
                }

                if (isset($filePath) && $filePath) {
                    TraderDealMessage::create([
                        'attachment_type' => 1,
                        'deal_id' => $id,
                        'msg' => $filePath,
                        'sender_id' => $request->user()->id,
                        'sender_type' => TraderDeal::TRADER_USER,
                    ]);
                } else {
                    return redirect()->back()->with(['alert-type' => 'error', 'message' => __('The Image Upload Fail')]);
                }
            } else {
                return redirect()->back()->with(['alert-type' => 'error', 'message' => __('The Image Required')]);
            }
        }

        if ($deal->type == TraderDeal::TYPE_SELL && $deal->status == TraderDeal::STATUS_CREATE && $status == TraderDeal::STATUS_TRADER_FUNDED) {
            if($this->checkTraderPaymentAccount($deal->payment_account_id, $deal, $request->user()->id) == false){
                return redirect()->back()->with(['alert-type' => 'error', 'message' => __('Payment Account Error')]);
            }

            $trader_payment_account_id = $request->input('trader_payment_account_id');
            $adJson = json_decode($deal->ad_json);

            $trader_payment_account = TraderPaymentAccount::where('id', (int) $trader_payment_account_id)->firstOrFail();

            if($deal->payment_account_id != $trader_payment_account_id){
                $deal->payment_account_id = $trader_payment_account_id;
                $deal->card_number = $trader_payment_account->card_number;
                $deal->payment_info = $trader_payment_account->payment_info;
                $adJson->card_number = $trader_payment_account->card_number;
                $adJson->payment_info = $trader_payment_account->payment_info;
                $deal->ad_json = json_encode($adJson);
                $deal->save();
            }
        }

        $result = $this->traderClient->setTraderDealStatus(['status' => $status, 'deal_id' => $id, 'description' => $description, 'customer_id' => $request->user()->id, 'customer_type' => TraderDeal::TRADER_USER]);
        if (!$result) {
            return redirect()->back()->with(['alert-type' => 'error', 'message' => __('The Deal Status Change Error')]);
        }
        return redirect()->back()->with(['alert-type' => 'success', 'message' => __('The Deal Status Change')]);
    }

    public function detail(Request $request, $id)
    {
        $deal = TraderDeal::where(['id' => $id, 'trader_id' => $request->user()->id])->with('statusHistory')->firstOrFail();
        $route_types = TraderAd::getRouteTypes();

        return view('trader_deal.detail', [
            'deal' => $deal,
            'route_types' => $route_types,
            'id' => $id,
        ]);
    }

    public function detailInfo(Request $request, $id)
    {
        $deal = TraderDeal::where(['id' => $id, 'trader_id' => $request->user()->id])->with(['fiat', 'currency'])->firstOrFail();

        $time = [
            'to_fund' => round($this->getLiveTime(
                $deal,
                config('app.trader_deals_create_live_time')
            ), 1),

            'to_complete' => round($this->getLiveTime(
                $deal,
                config('app.trader_deals_funded_live_time')
            ), 1)
        ];

        $payment_completed_at = null;
        if ($historyCompleted = TraderDealHistory::where(['deal_id' => $id, 'status' => TraderDeal::STATUS_PAYMENT_COMPLETED_USER])->first()) {
            $payment_completed_at = $historyCompleted->created_at;
        }

        $deal->type_text = __(TraderAd::getTypes()[$deal->type]);
        $deal->amount_currency = price_format($deal->amount_currency, 8);
        $deal->amount_fiat = price_format($deal->amount_fiat, 2);
        $deal->rate = price_format($deal->rate, 2);
        $deal->xcreated_at = datetimeFormat($deal->created_at);
        $deal->xupdated_at = datetimeFormat($deal->updated_at);
        $deal->xpayment_completed_at = datetimeFormat($payment_completed_at);
        $deal->status_text = __('status_' . $deal->status);
        $deal->status_route = route('trader_deal.status', ['id' => $id]);

        $adJson = json_decode($deal->ad_json);

        $payment_system_type = null;
        if ($adJson->payment_system_type_id ?? '') {
            $payment_system_type = PaymentSystemType::find($adJson->payment_system_type_id);
        }

        $payment_system = null;
        if ($adJson->payment_system_id ?? '') {
            $payment_system = TraderPaymentSystem::find($adJson->payment_system_id);
        }

        $ad = null;
        if (in_array($deal->status, [TraderDeal::STATUS_CREATE])) {
            $ad = TraderAd::find($deal->ad_id);
        }

        return response()->json([
            'deal' => [
                'id' => $deal->id,
                'type' => $deal->type,
                'card_number' => $deal->card_number,
                'payment_info' => $deal->payment_info,
                'amount_currency' => $deal->amount_currency,
                'amount_fiat' => $deal->amount_fiat,
                'rate' => $deal->rate,
                'status' => $deal->status,
                'xcreated_at' => $deal->xcreated_at,
                'xupdated_at' => $deal->xupdated_at,
                'xpayment_completed_at' => $deal->xpayment_completed_at,
                'type_text' => $deal->type_text,
                'status_text' => $deal->status_text,
                'status_route' => $deal->status_route,
            ],
            'req' => $ad ? [
                'payment_accounts' => $ad->payment_accounts,
            ] : null,
            'currency' => [
                'name' => $deal->currency->name,
                'asset' => $deal->currency->asset,
            ],
            'fiat' => [
                'name' => $deal->fiat->name,
                'asset' => $deal->fiat->asset,
            ],
            'payment_system_type' => $payment_system_type ? [
                'name' => $payment_system_type->name,
                'obj_name' => $payment_system_type->obj_name,
            ] : (object) [],
            'payment_system' => $payment_system ? [
                'name' => $payment_system->name,
            ] : (object) [],
            'time' => $time,
            'balance' => $request->user()->getTotalBalance($deal->currency_id),
            'consta' => [
                'TYPE_BUY' => TraderDeal::TYPE_BUY,
                'TYPE_SELL' => TraderDeal::TYPE_SELL,
                'STATUS_CREATE' => TraderDeal::STATUS_CREATE,
                'STATUS_COMPLETED' => TraderDeal::STATUS_COMPLETED,
                'STATUS_TRADER_FUNDED' => TraderDeal::STATUS_TRADER_FUNDED,
                'STATUS_PAYMENT_COMPLETED_USER' => TraderDeal::STATUS_PAYMENT_COMPLETED_USER,
                'STATUS_PAYMENT_COMPLETED_TRADER' => TraderDeal::STATUS_PAYMENT_COMPLETED_TRADER,
                'STATUS_DISPUTED' => TraderDeal::STATUS_DISPUTED,
                'STATUS_CANCELED_BY_REPLY_TIMEOUT' => TraderDeal::STATUS_CANCELED_BY_REPLY_TIMEOUT,
                'STATUS_CANCELED_TIMEOUT' => TraderDeal::STATUS_CANCELED_TIMEOUT,
                'STATUS_CANCELED_SD' => TraderDeal::STATUS_CANCELED_SD,
                'STATUS_CANCELED_TRADER' => TraderDeal::STATUS_CANCELED_TRADER,
                'STATUS_CANCELED_USER' => TraderDeal::STATUS_CANCELED_USER,
                'STATUS_CANCELED' => TraderDeal::STATUS_CANCELED
            ],
            'csrf' => csrf_token()
        ]);
    }

    public function messages(Request $request, $id)
    {
        $deal = TraderDeal::where(['id' => $id, 'trader_id' => $request->user()->id])->with('statusHistory')->firstOrFail();

        $dealMessage = TraderDealMessage::where('deal_id', '=', $id)->orderBy('created_at', 'desc')->get();
        $senderType = $this->traderClient->getCustomerType();

        foreach ($dealMessage as $unit) {
            if ($unit->sender_type == TraderDeal::SYSTEM) {
                $unit->sender_name = $senderType[TraderDeal::SYSTEM]['label'];
            } else {
                $instance = app($senderType[$unit->sender_type]['instance']);
                $unit->sender_name = $instance::where('id', '=', $unit->sender_id)->first()->name ?? '';
            }
            if ($unit->attachment_type) {
                $unit->attachment = route('trader_deal.message.attachment', ['id' => $unit->id]);
            }

            if ($senderType[$unit->sender_type]['instance'] == 'App\Models\Merchant') {
                $unit->sender_name = __("Client");
            } elseif ($senderType[$unit->sender_type]['instance'] == 'App\Models\SdUser') {
                $unit->sender_name = __("Arbiter");
            } else {
                $unit->sender_name = __($senderType[$unit->sender_type]["label"]);
            }

            $unit->xcreated_at = datetimeFormat($unit->created_at);

            foreach (['updated_at', 'viewed', 'viewed_at', 'sender_id', 'sender_type'] as $field) {
                unset($unit->$field);
            }
        }

        return response()->json([
            'messages' => $dealMessage
        ]);
    }

    private function getLiveTime($deal, $time)
    {
        if (!isset($deal->statusHistory)) {
            return false;
        }

        $expired_at = Carbon::parse($deal->statusHistory->created_at);
        $expired_at = $expired_at->addMinutes($time);
        $expired = Carbon::now()->diffInMinutes($expired_at, false);

        return $expired;
    }

    public function messageSend(Request $request, $id)
    {
        $message = $request->input('message', null);

        $messages = [];
        $rules = [
            'document' => 'nullable|image|mimes:jpeg,png,jpg,gif',
            'message' => 'nullable|string',
            'id' => 'required|numeric',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        if ($message) {
            TraderDealMessage::create([
                'attachment_type' => 0,
                'deal_id' => $id,
                'msg' => $message,
                'sender_id' => $request->user()->id,
                'sender_type' => TraderDeal::TRADER_USER,
            ]);
        }

        if ($request->hasFile('document')) {
            $file = $request->file('document');

            if ($file) {
                $filePath = $this->traderClient->saveFile($file, $file->getClientOriginalName());
            }

            if (isset($filePath) && $filePath) {
                TraderDealMessage::create([
                    'attachment_type' => 1,
                    'deal_id' => $id,
                    'msg' => $filePath,
                    'sender_id' => $request->user()->id,
                    'sender_type' => TraderDeal::TRADER_USER,
                ]);
            } else {
                return redirect()->route('trader_deal.detail', ['id' => $id])->with(['alert-type' => 'error', 'message' => __('The Deal Message Send Fail')]);
            }
        }
        return redirect()->route('trader_deal.detail', ['id' => $id])->with(['alert-type' => 'success', 'message' => __('The Deal Message Send Success')]);
    }

    public function attachment(Request $request, $id)
    {
        $message = TraderDealMessage::where(['id' => $id])->where('attachment_type', '>', 0)->firstOrFail();

        $deal = TraderDeal::where(['id' => $message->deal_id])
            ->with('ad')->firstOrFail();

        if ($deal->ad->trader_id != $request->user()->id) {
            abort(403, 'Unauthorized action.');
        }

        $content = $this->traderClient->getFile(['name' => $message->msg]);
        $fh = fopen('php://memory', 'w+b');
        fwrite($fh, $content);
        $contentType = mime_content_type($fh);
        fclose($fh);

        return response($content, 200)->header('Content-type', $contentType);
    }

    private function checkTraderPaymentAccount($id, $deal, $user_id)
    {
        $account = TraderPaymentAccount::where([
            'id' => $id,
            'trader_id' => $user_id,
            'status' => TraderPaymentAccount::STATUS_ACTIVE
        ])->firstOrFail();

        if($account &&
            $deal->payment_system_id == $account->payment_system_id
            && $deal->fiat_id == $account->trader_fiat_id
        ) {
            return true;
        }

        return false;
    }
}

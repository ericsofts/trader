<?php

namespace App\Http\Controllers;

use App\Models\TraderDealStatusCode;
use DateTimeZone;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use BaconQrCode\Renderer\Image\ImagickImageBackEnd;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use App\Models\TraderCurrencyAddress;
use App\Models\TraderCurrency;
use App\Http\Libs\Api\Xpubd as XpubdClient;
use App\Models\TraderFiat;
use Illuminate\Support\Facades\Validator;

class TraderController extends Controller
{
    private $xpubdClient;

    public function __construct(
        XpubdClient $xpubdClient
    )
    {
        $this->xpubdClient = $xpubdClient;
    }

    public function profile(Request $request) {
        $trader = $request->user();
        
        $savedStatuses = TraderDealStatusCode::where('trader_id', $request->user()->id)->get();
        $codeStatuses = array_flip(TraderDealStatusCode::getAvalableStatusCode());
        $avalableStatusesData = TraderDealStatusCode::getAvalableStatusData();
        foreach ($savedStatuses as $savedStatus){
            $avalableStatusesData[$codeStatuses[$savedStatus->status]]['value'] = $savedStatus->code;
        }

        $timezone = $trader->timezone;
        $timezoneList = DateTimeZone::listIdentifiers(DateTimeZone::ALL);

        $currencyList = TraderFiat::where(['status' => 1])->orderBy('name')->get();
        $default_currency = $trader->default_currency ?? config('app.default_currency');

        return view('trader.profile', [
            'trader' => $trader,
            'avalableStatusesData' => $avalableStatusesData,
            'timezoneList' => $timezoneList,
            'timezone' => $timezone,
            'currencyList' => $currencyList,
            'default_currency' => $default_currency,
        ]);
    }

    public function profileSave(Request $request) {
        $trader = $request->user();
        $messages = [];

        $rules = [
            'code.*' => 'nullable|string',
            'timezone' => 'nullable|string'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $this->saveCode($request);
        $trader->default_currency = $request->input('default_currency');
        $trader->timezone = $request->input('timezone');
        $trader->telegram = $request->input('telegram');
        $trader->save();

        return redirect()->back()->with(['alert-type' => 'success', 'message' => __('Profile Saved')]);
    }

    public function balances(Request $request)
    {
        $trader_currency = TraderCurrency::where('status', 1)->get();

        foreach ($trader_currency as &$unit) {
            $unit->balance_amount = $request->user()->getTotalBalance($unit->id);
            $unit->currency_address = TraderCurrencyAddress::where([
                'trader_id' => $request->user()->id,
                'currency_id' => $unit->id
            ])
                ->whereNotNull('address')->first();
        }

        return view('trader.balances', [
            'trader_currency' => $trader_currency
        ]);
    }

    public function get_address(Request $request)
    {
        $currency = TraderCurrency::findOrFail($request->input('currency_id'));

        $oldAddress = TraderCurrencyAddress::where([
            'trader_id' => Auth::user()->id,
            'currency_id' => $currency->id,
        ])->first();

        $newAddress = TraderCurrencyAddress::create([
            'trader_id' => Auth::user()->id,
            'currency_id' => $currency->id,
        ]);

        $result = $this->xpubdClient->addrNew([
            'trader', $currency->crypto_asset, $newAddress->id
        ]);

        if (!$result) {
            return redirect()->back()->with(['alert-type' => 'error', 'message' => __('Address get Error')]);
        }

        $newAddress->address = $result;
        $newAddress->save();

        if ($oldAddress) {
            $oldAddress->delete();
        }

        return redirect()->back();
    }

    public function address_qr(Request $request, $address)
    {
        $size = 200;
        $imageBackEnd = new ImagickImageBackEnd();
        $renderer = new ImageRenderer(
            (new RendererStyle($size))->withSize($size),
            $imageBackEnd
        );

        $bacon = new Writer($renderer);

        $data = $bacon->writeString(
            $address,
            'utf-8'
        );

        $response = Response::make($data, 200);
        $response->header("Content-Type", "image/png");
        return $response;
    }

    private function saveCode($request){
        $trader = $request->user();
        foreach ($request->input('code') as $key=>$code){
            $value = TraderDealStatusCode::firstOrCreate([
                'trader_id' => $trader->id,
                'status' => $key
            ],
            [
                'code' => $code
            ]);
            $value->code = $code;
            $value->save();
        }
    }

}

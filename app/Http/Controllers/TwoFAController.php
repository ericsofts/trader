<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PragmaRX\Google2FALaravel\Facade as Google2FA;

class TwoFAController extends Controller
{
    public function index(Request $request)
    {
        $enable2fa = $request->user()->enable_2fa;
        $secret = $request->user()->secret_2fa;
        
        if(!$secret){
            $secret =  Google2FA::generateSecretKey();
            $request->user()->secret_2fa = $secret;
            $request->user()->save();
        }

        $qr = Google2FA::getQRCodeInline(
            config('app.name'),
            $request->user()->email,
            $secret,
        );

        return view('2fa.index', compact('secret', 'qr', 'enable2fa'));
    }

    public function status(Request $request)
    {
        return response()->json([
            'status' => !empty($request->user()->secret_2fa) && $request->user()->enable_2fa
        ]);
    }

    public function configure(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'code' => ['required', 'string', function ($attribute, $value, $fail) use ($request) {
                if (!Google2FA::verifyKey($request->user()->secret_2fa, $value)) {
                    $fail(__('The code you provided is not valid.'));
                }
            }],
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput()
                ->with(['alert-type' => 'error', 'message' => __('Two Factor Authentication has not been configured.')]);
        }

        $request->user()->enable_2fa = true;
        $request->user()->save();

        return redirect()->back()->with(['alert-type' => 'success', 'message' => __('Two Factor Authentication has been successfully configured.')]);
    }

    public function disable(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'disable_code' => ['required', 'string', function ($attribute, $value, $fail) use ($request) {
                if (!Google2FA::verifyKey($request->user()->secret_2fa, $value)) {
                    $fail(__('The code you provided is not valid.'));
                }
            }],
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $validator->errors(),
                'message' => __('Two Factor Authentication has not been disabled.')
            ]);
        }
        
        $request->user()->enable_2fa = false;
        $request->user()->save();

        return response()->json([
            'status' => true,
            'message' => __('Two Factor Authentication has been successfully disabled.')
        ]);
    }

    public function generateNew(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'generate_code' => ['required', 'string', function ($attribute, $value, $fail) use ($request) {
                if (!Google2FA::verifyKey($request->user()->secret_2fa, $value)) {
                    $fail(__('The code you provided is not valid.'));
                }
            }],
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $validator->errors(),
                'message' => __('Two Factor Authentication secret has not been generated.')
            ]);
        }

        $request->user()->enable_2fa = false;
        $request->user()->secret_2fa = null;
        $request->user()->save();

        $request->session()->flashInput(['enable_2fa' => true]);

        return response()->json([
            'status' => true,
            'message' => __('Two Factor Authentication secret has been successfully generated.')
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\TraderAd;
use App\Models\TraderCurrency;
use App\Models\TraderCurrencyAddress;
use App\Models\TraderDeal;
use App\Models\TraderRate;
use App\Models\TraderFiat;
use App\Models\TraderBalance;
use App\Models\TraderBalanceHistory;
use App\Models\TraderDealHistory;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        return view('dashboard');
    }

    public function balance(Request $request)
    {
        $traderCurrency = TraderCurrency::where('status', 1)->get();

        $default_currency = $request->user()->default_currency;

        $from_date = now()->timezone($request->user()->timezone ?? config('app.default.timezone'))->startOfDay()->timezone('UTC');
        $to_date = now()->timezone($request->user()->timezone ?? config('app.default.timezone'))->endOfDay()->timezone('UTC');

        foreach ($traderCurrency as &$unit) {
            $unit->balance_amount = $request->user()->getTotalBalance($unit->id);
            $unit->currency_address = TraderCurrencyAddress::where([
                'trader_id' => $request->user()->id,
                'currency_id' => $unit->id
            ])
                ->whereNotNull('address')->first();

            $unit->show_balance_amount = $unit->balance_amount;

            $unit->show_currency = [
                'id' => $unit->id, 'asset' => $unit->asset, 'name' => $unit->name
            ];

            $credit = TraderBalanceHistory::where([
                'balance_type' => TraderBalance::TYPE_NORMAL,
                'trader_id' => $request->user()->id,
                'currency_id' => $unit->id,
                'type' => TraderBalanceHistory::TYPE_CREDIT
            ])->where('created_at', '>=', $from_date)->where('created_at', '<=', $to_date)->sum('amount');

            $debit = TraderBalanceHistory::where([
                'balance_type' => TraderBalance::TYPE_NORMAL,
                'trader_id' => $request->user()->id,
                'currency_id' => $unit->id,
                'type' => TraderBalanceHistory::TYPE_DEBIT
            ])->where('created_at', '>=', $from_date)->where('created_at', '<=', $to_date)->sum('amount');

            $unit->show_balancechange = $credit - $debit;
            $unit->balancechange = $unit->show_balancechange;

            if ($default_currency) {
                $urate = TraderRate::where('fiat_id', $default_currency)->first();
                if ($urate) {
                    $ucurr = TraderFiat::find($urate->fiat_id);
                    $unit->show_currency = [
                        'id' => $ucurr->id, 'asset' => $ucurr->asset, 'name' => $ucurr->name
                    ];

                    $unit->show_balance_amount = number_format($unit->balance_amount * $urate->rate, $ucurr->precision);
                    $unit->show_balancechange = number_format($unit->show_balancechange * $urate->rate, $ucurr->precision);
                }
            }
        }

        return response()->json([
            'balance' => $traderCurrency,
        ]);
    }

    public function dealStat(Request $request)
    {
        $from_date = now()->timezone($request->user()->timezone ?? config('app.default.timezone'))->startOfDay()->timezone('UTC');
        $to_date = now()->timezone($request->user()->timezone ?? config('app.default.timezone'))->endOfDay()->timezone('UTC');

        $completedSellQuery = TraderDealHistory::where([
            'trader_deals.trader_id' => $request->user()->id,
            'trader_deal_histories.status' => TraderDeal::STATUS_COMPLETED
        ])
            ->join('trader_deals', 'trader_deals.id', '=', 'trader_deal_histories.deal_id')
            ->where('trader_deals.type', TraderDeal::TYPE_SELL);

        $completed_sell_total = $completedSellQuery->count();
        $completed_sell = $completedSellQuery->where('trader_deal_histories.created_at', '>=', $from_date)->where('trader_deal_histories.created_at', '<=', $to_date)->count();

        $completedBuyQuery = TraderDealHistory::where([
            'trader_deals.trader_id' => $request->user()->id,
            'trader_deal_histories.status' => TraderDeal::STATUS_COMPLETED
        ])
            ->join('trader_deals', 'trader_deals.id', '=', 'trader_deal_histories.deal_id')
            ->where('trader_deals.type', TraderDeal::TYPE_BUY);

        $completed_buy_total = $completedBuyQuery->count();
        $completed_buy = $completedBuyQuery->where('trader_deal_histories.created_at', '>=', $from_date)->where('trader_deal_histories.created_at', '<=', $to_date)->count();

        return response()->json([
            'completed_sell_total' => $completed_sell_total,
            'completed_sell' => $completed_sell,
            'completed_buy_total' => $completed_buy_total,
            'completed_buy' => $completed_buy,
        ]);
    }

    public function deals(Request $request)
    {
        $types = TraderAd::getTypes();

        $query = TraderDeal::where(['trader_id' => $request->user()->id])
            ->whereNotIn('status', [
                TraderDeal::STATUS_COMPLETED,
                TraderDeal::STATUS_CANCELED_BY_REPLY_TIMEOUT,
                TraderDeal::STATUS_CANCELED_TIMEOUT,
                TraderDeal::STATUS_CANCELED_SD,
                TraderDeal::STATUS_CANCELED_TRADER,
                TraderDeal::STATUS_CANCELED_USER,
                TraderDeal::STATUS_CANCELED,
            ])
            ->orderBy('status', 'asc')
            ->orderBy('created_at', 'desc')
            ->with(['fiat', 'payment_system', 'currency']);
        $traderDeals = $query->get();

        foreach ($traderDeals as $i => $unit) {
            $traderDeals[$i]->type_text = __($types[$unit->type]);
            $traderDeals[$i]->link = route('trader_deal.detail', ['id' => $unit->id]);
            $traderDeals[$i]->status_text = __('status_' . $unit->status);
            $traderDeals[$i]->xcreated_at = datetimeFormat($unit->created_at);
            $traderDeals[$i]->amount_fiat = number_format($unit->amount_fiat, 2);
            $traderDeals[$i]->amount_currency = number_format($unit->amount_currency, 8);
            unset($traderDeals[$i]->ad_json);
        }

        return response()->json([
            'deals' => $traderDeals,
        ]);
    }

    public function psInfo(Request $request)
    {
        $time_from = $request->cookie('psInfo_time', time());

        $tDeals = TraderDealHistory::join('trader_deals', 'trader_deals.id', '=', 'trader_deal_histories.deal_id')
            ->where('trader_deals.trader_id', $request->user()->id)
            ->where('trader_deal_histories.created_at', '>=', Carbon::createFromTimestamp($time_from - 1))
            ->select('trader_deal_histories.id', 'deal_id', 'trader_deal_histories.status as history_status')
            ->get();

        $updatedDeals = [];

        foreach ($tDeals as $unit) {
            if (!$unit->deal) {
                continue;
            }

            $obj = [
                'id' => $unit->id,
                'deal_id' => $unit->deal->id,
                'link' => route('trader_deal.detail', ['id' => $unit->deal->id])
            ];

            if ($unit->history_status == 0) {
                $text = __('New deal text');
            } else {
                $text = __('Changed deal text');
            }
            $text = preg_replace('/%ID%/', "<strong>{$unit->deal->id}</strong>", $text);
            $text = preg_replace('/%STATUS%/', "<strong>" . __('status_' . $unit->deal->status) . "</strong>", $text);
            $obj['text'] = $text;

            $updatedDeals[] = $obj;
        }
        return response()->json([
            'updatedDeals' => $updatedDeals,
            'timeFrom' => Carbon::createFromTimestamp($time_from),
            'now' => time()
        ])->cookie('psInfo_time', time(), 60 * 24);
    }
}

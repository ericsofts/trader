<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use PragmaRX\Google2FALaravel\Facade as Google2FA;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.login');
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LoginRequest $request)
    {
        $request->authenticate();
        if ($request->user()->enable_2fa && !empty($request->user()->secret_2fa)) {
            $request->session()->put('login_verify2fa.user_id', $request->user()->id);
            $request->session()->put('login_verify2fa.secret', $request->user()->secret_2fa);
            Auth::guard('web')->logout();
            return redirect()->route('login.verify2fa');
        }
        $request->session()->regenerate();

        return redirect()->intended(RouteServiceProvider::HOME);
    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }

    public function showVerify2fa()
    {
        $secret2fa = session()->get('login_verify2fa.secret');
        $loaded = session()->get('login_verify2fa.loaded');
        if (!$secret2fa || $loaded) {
            session()->remove('login_verify2fa');
            return redirect()->route('login');
        }
        session()->put('login_verify2fa.loaded', true);
        return view('auth.verify2fa');
    }

    public function verify2fa(Request $request)
    {
        $secret2fa = $request->session()->pull('login_verify2fa.secret');
        $userId = $request->session()->pull('login_verify2fa.user_id');
        $request->session()->remove('login_verify2fa');
        $validator = Validator::make($request->input(), [
            'code' => ['required', 'string', function ($attribute, $value, $fail) use ($secret2fa) {
                if (!Google2FA::verifyKey($secret2fa, $value)) {
                    $fail(__('The code you provided is not valid.'));
                }
            }],
        ]);
        if ($validator->fails()) {
            $user = new User();
            $user->id = $userId;
            return redirect()->route('login')->with(['alert-type' => 'error', 'message' => __('The code you provided is not valid.')]);
        }
        Auth::loginUsingId($userId);
        return redirect()->intended(RouteServiceProvider::HOME);
    }
}

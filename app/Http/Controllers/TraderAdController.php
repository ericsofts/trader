<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\TraderPaymentAccount;
use App\Models\TraderCurrency;
use App\Models\TraderFiat;
use App\Models\TraderAd;
use App\Models\TraderRateSource;
use App\Models\TraderRate;
use App\Models\PaymentSystemType;

class TraderAdController extends Controller
{

    public function index(Request $request, $type)
    {
        $types = TraderAd::getTypes();
        $route_types = TraderAd::getRouteTypes();

        $type = array_flip($route_types)[$type];

        $limit = config('app.default.pagination_limit');
        $status = $request->input('status');

        $query = TraderAd::where(['trader_id' => Auth::user()->id]);

        if (!is_null($status) && in_array($status, [0, 1])) {
            $query->where('status', '=', $status);
        }
        if (!is_null($type) && in_array($type, [0, 1])) {
            $query->where('type', '=', $type);
        }

        $query->with(['fiat', 'payment_system', 'currency', 'payment_accounts' =>  function ($query) {
            $query->where('status', TraderPaymentAccount::STATUS_ACTIVE)->orderBy('name', 'asc');
        }]);
        $query->orderBy('id', 'asc');

        $trader_ads = $query->paginate($limit)->withQueryString();

        return view('trader_ad.index', [
            'types' => $types,
            'route_types' => $route_types,
            'status' => $status,
            'type' => $type,
            'trader_ads' => $trader_ads
        ]);
    }

    public function new(Request $request, $type)
    {
        $data = [];

        $data['all_fiats'] = TraderFiat::with('payment_systems', 'payment_systems.available_payment_types')->where('status', '1')->get(['id', 'asset', 'name']);
        $data['all_currencies'] = TraderCurrency::where('status', 1)->get(['id', 'asset', 'name']);
        $data['all_rate_sources'] = TraderRateSource::orderBy('id', 'asc')->get();
        $data['all_payment_system_types'] = PaymentSystemType::orderBy('id', 'asc')->get();

        $data['types'] = TraderAd::getTypes();
        $data['route_types'] = TraderAd::getRouteTypes();
        $data['type'] = array_flip($data['route_types'])[$type];

        return view('trader_ad.new', $data);
    }

    public function edit(Request $request, $id)
    {
        $trader_ad = TraderAd::where(['id' => $id, 'trader_id' => $request->user()->id])->firstOrFail();

        $data['ad'] = $trader_ad;

        $data['all_fiats'] = TraderFiat::with('payment_systems', 'payment_systems.available_payment_types')->where('status', '1')->get(['id', 'asset', 'name']);
        $data['all_currencies'] = TraderCurrency::where('status', 1)->get(['id', 'asset', 'name']);
        $data['all_rate_sources'] = TraderRateSource::orderBy('id', 'asc')->get();
        $data['all_payment_system_types'] = PaymentSystemType::orderBy('id', 'asc')->get();

        $data['types'] = TraderAd::getTypes();
        $data['route_types'] = TraderAd::getRouteTypes();
        $data['type'] = $trader_ad->type;

        return view('trader_ad.edit', $data);
    }

    public function availableAccounts(Request $request)
    {
        $payment_system_id = $request->input('payment_system_id');
        $payment_system_type_id = $request->input('payment_system_type_id');
        $trader_ad_id = $request->input('trader_ad_id');

        return response()->json(
            TraderPaymentAccount::availableAccounts($payment_system_id, $payment_system_type_id, $trader_ad_id)
        );
    }

    public function create(Request $request)
    {
        $messages = [];

        $type = $request->input('type');
        $rate_type = $request->input('rate_type');
        $min_amount = $request->input('min_amount');

        $rules = [
            'type' => 'required|integer',
            'fiat_id' => 'required|integer',
            'currency_id' => 'required|integer',
            'payment_system_id' => 'required|integer',
            'min_amount' => 'required|numeric',
            'rate_type' => 'required|integer',
            'rate' => 'required|numeric|regex:/^\d+(\.\d{1,2})?$/',
            'auto_acceptance' => 'required|boolean',
        ];

        $rules['max_amount'] = ['required', 'numeric', function ($attribute, $value, $fail) use ($min_amount) {
            if ($value < $min_amount) {
                $fail(':attribute must be equal or greater than min amount');
            }
        }];

        if ($rate_type == TraderAd::RATE_PERCENTAGE) {
            $rules['rate_source_id'] = 'required|numeric';
            $rules['rate'] = 'required|numeric|regex:/^-*\d+(\.\d{1,2})?$/';
        }

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $trader_ad = $this->createTraderAd($request);
        if ($trader_ad) {
            return redirect()->route('trader_ad.index', ['type' => TraderAd::getRouteTypes()[$type]])->with(['alert-type' => 'success', 'message' => __('Ad Created')]);
        }

        return redirect()->route('trader.index', ['type' => TraderAd::getRouteTypes()[$type]])->with(['alert-type' => 'error', 'message' => __('Ad Create Error')]);
    }

    public function update(Request $request, $id)
    {
        $trader_ad = TraderAd::where(['id' => $id, 'trader_id' => Auth::user()->id])->firstOrFail();

        $messages = [];

        $type = $request->input('type');
        $rate_type = $request->input('rate_type');
        $min_amount = $request->input('min_amount');

        $rules = [
            'type' => 'required|integer',
            'fiat_id' => 'required|integer',
            'currency_id' => 'required|integer',
            'payment_system_id' => 'required|integer',
            'min_amount' => 'required|numeric',
            'rate_type' => 'required|integer',
            'rate' => 'required|numeric|regex:/^\d+(\.\d{1,2})?$/',
            'auto_acceptance' => 'required|boolean',
        ];

        $rules['max_amount'] = ['required', 'numeric', function ($attribute, $value, $fail) use ($min_amount) {
            if ($value < $min_amount) {
                $fail(':attribute must be equal or greater than min amount');
            }
        }];

        if ($rate_type == TraderAd::RATE_PERCENTAGE) {
            $rules['rate_source_id'] = 'required|numeric';
            $rules['rate'] = 'required|numeric|regex:/^-*\d+(\.\d{1,2})?$/';
        }

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        if ($trader_ad) {
            $this->updateTraderAd($request, $trader_ad);
            return redirect()->route('trader_ad.index', ['type' => TraderAd::getRouteTypes()[$type]])->with(['alert-type' => 'success', 'message' => __('Ad Updated')]);
        }

        return redirect()->route('trader_ad.index', ['type' => TraderAd::getRouteTypes()[$type]])->with(['alert-type' => 'error', 'message' => __('Ad Update Error')]);
    }

    protected function createTraderAd(Request $request)
    {
        $data = [
            'trader_id' => Auth::user()->id,
            'type' => $request->input('type'),
            'fiat_id' => $request->input('fiat_id'),
            'currency_id' => $request->input('currency_id'),
            'payment_system_id' => $request->input('payment_system_id'),
            'payment_system_type_id' => $request->input('payment_system_type_id'),
            'min_amount' => $request->input('min_amount'),
            'max_amount' => $request->input('max_amount'),
            'rate_type' => $request->input('rate_type'),
            'rate_stop' => $request->input('rate_stop'),
            'rate_source_id' => $request->input('rate_source_id'),
            'rate' => $request->input('rate'),
            'status' => $request->input('status') ? $request->input('status') : 0,
            'account_order' => $request->input('account_order'),
            'auto_acceptance' => $request->input('auto_acceptance'),
        ];
        $trader_ad = TraderAd::create($data);

        $trader_ad->payment_accounts()->sync($request->payment_accounts_ids);

        return $trader_ad;
    }

    protected function updateTraderAd(Request $request, $trader_ad)
    {
        $trader_ad->type = $request->input('type');
        $trader_ad->fiat_id = $request->input('fiat_id');
        $trader_ad->currency_id = $request->input('currency_id');
        $trader_ad->payment_system_id = $request->input('payment_system_id');
        $trader_ad->payment_system_type_id = $request->input('payment_system_type_id');
        $trader_ad->min_amount = $request->input('min_amount');
        $trader_ad->max_amount = $request->input('max_amount');
        $trader_ad->rate_type = $request->input('rate_type');
        $trader_ad->rate_stop = $request->input('rate_stop');
        $trader_ad->rate_source_id = $request->input('rate_source_id');
        $trader_ad->rate = $request->input('rate');
        $trader_ad->status = $request->input('status');
        $trader_ad->account_order = $request->input('account_order');
        $trader_ad->auto_acceptance = $request->input('auto_acceptance');

        $trader_ad->payment_accounts()->sync($request->payment_accounts_ids);

        $trader_ad->save();
    }

    public function rates(Request $request)
    {
        $rate_source_id = $request->input('rate_source_id');
        $currency_id = $request->input('currency_id');
        $fiat_id = $request->input('fiat_id');

        $rate = TraderRate::where([
            'rate_source_id' => $rate_source_id,
            'currency_id' => $currency_id,
            'fiat_id' => $fiat_id
        ])->firstOrFail();

        return price_format($rate->rate, 2);
    }

    public function enable(Request $request, $id)
    {
        $trader_ad = TraderAd::where(['id' => $id, 'trader_id' => Auth::user()->id])->firstOrFail();
        $trader_ad->status = TraderAd::STATUS_ACTIVE;
        $trader_ad->save();
        return redirect()->back();
    }

    public function disable(Request $request, $id)
    {
        $trader_ad = TraderAd::where(['id' => $id, 'trader_id' => Auth::user()->id])->firstOrFail();
        $trader_ad->status = TraderAd::STATUS_INACTIVE;
        $trader_ad->save();

        return redirect()->back();
    }

    public function enableall(Request $request)
    {
        $trader_ad = TraderAd::where(['trader_id' => Auth::user()->id])->update(['status' => TraderAd::STATUS_ACTIVE]);
        return redirect()->back();
    }

    public function disableall(Request $request)
    {
        $trader_ad = TraderAd::where(['trader_id' => Auth::user()->id])->update(['status' => TraderAd::STATUS_INACTIVE]);
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use App\Lib\LuhnAlgorithm;
use Illuminate\Http\Request;
use App\Models\TraderPaymentAccount;
use App\Models\TraderPaymentSystem;
use App\Models\PaymentSystemType;
use App\Models\TraderFiat;
use App\Models\TraderPaymentSystemTypeRelation;
use Illuminate\Support\Facades\Validator;


class TraderPaymentAccountController extends Controller
{

    public function index(Request $request)
    {
        $trader = $request->user();
        return view('payment-system-accounts.index', [
            'accounts' => $trader->payment_accounts,
        ]);
    }

    public function new(Request $request)
    {
        $fiats = TraderFiat::where(['status' => 1])->get();
        return view('payment-system-accounts.create', [
            'fiats' => $fiats
        ]);
    }

    public function add(Request $request)
    {
        $messages = [];
        $rules = [
            'card_number' => 'required|integer',
            'payment_system_type_id' => 'required|integer',
            'payment_system_id' => 'required',
            'trader_fiat_id' => 'required|integer',
        ];
        if (!is_null($request->input('payment_system_type_id'))) {
            $rules = $this->addCardNumberRule($rules, $request);
        }

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $trader = $request->user();
        $paysysaccount = new TraderPaymentAccount;
        $paysysaccount->name = $request->input('name');
        $paysysaccount->trader_id = $trader->id;
        $paysysaccount->trader_fiat_id = (int) $request->input('trader_fiat_id');
        $paysysaccount->payment_system_id = (int) $request->input('payment_system_id');
        $paysysaccount->payment_system_type_id = (int) $request->input('payment_system_type_id');
        $paysysaccount->limit_amount = $request->input('limit_amount') ?? 0;
        $paysysaccount->card_number = $request->input('card_number');
        $paysysaccount->payment_info = $request->input('payment_info');

        if ($paysysaccount->save()) {
            return redirect()->route('trader.payment-system-accounts')->with(['alert-type' => 'success', 'message' => __('Payment System Account Created')]);
        }

        return redirect()->back()->with(['alert-type' => 'error', 'message' => __('Error')]);
    }

    public function edit(Request $request, $id)
    {
        $trader = $request->user();
        $trader_account = TraderPaymentAccount::where(['id' => $id, 'trader_id' => $trader->id])->firstOrFail();

        return view('payment-system-accounts.edit', [
            'account' => $trader_account,
        ]);
    }

    public function update(Request $request, $id)
    {
        $trader = $request->user();
        $trader_account = TraderPaymentAccount::where(['id' => $id, 'trader_id' => $trader->id])->firstOrFail();

        $messages = [];
        $rules = [
            'payment_system_type_id' => 'required|integer',
            'trader_fiat_id' => 'required|integer',
        ];
        $rules = $this->addCardNumberRule($rules, $request);

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        $trader_account->limit_amount = $request->input('limit_amount') ?? 0;
        $trader_account->card_number = $request->input('card_number');
        $trader_account->name = $request->input('name');

        if ($trader_account->save()) {
            return redirect()->back()->with(['alert-type' => 'success', 'message' => __('Payment System Account Saved')]);
        }

        return redirect()->back()->with(['alert-type' => 'error', 'message' => __('Error')]);
    }

    public function disable(Request $request, $id)
    {
        $trader = $request->user();
        $trader_account = TraderPaymentAccount::where(['id' => $id, 'trader_id' => $trader->id])->firstOrFail();
        $trader_account->status = TraderPaymentAccount::STATUS_INACTIVE;
        if ($trader_account->save()) {
            return redirect()->back()->with(['alert-type' => 'success', 'message' => __('Disabled')]);
        }
        return redirect()->back()->with(['alert-type' => 'error', 'message' => __('Error')]);
    }

    public function enable(Request $request, $id)
    {
        $trader = $request->user();
        $trader_account = TraderPaymentAccount::where(['id' => $id, 'trader_id' => $trader->id])->firstOrFail();
        $trader_account->status = TraderPaymentAccount::STATUS_ACTIVE;

        if ($trader_account->save()) {
            return redirect()->back()->with(['alert-type' => 'success', 'message' => __('Enabled')]);
        }

        return redirect()->back()->with(['alert-type' => 'error', 'message' => __('Error')]);
    }

    public function delete(Request $request, $id)
    {
        $trader = $request->user();
        $trader_account = TraderPaymentAccount::where(['id' => $id, 'trader_id' => $trader->id])->firstOrFail();
        if ($trader_account->delete()) {
            return redirect()->route('trader.payment-system-accounts')->with(['alert-type' => 'success', 'message' => __('Deleted')]);
        }
        return redirect()->back()->with(['alert-type' => 'error', 'message' => __('Error')]);
    }

    public function reset(Request $request, $id)
    {
        $trader = $request->user();
        $trader_account = TraderPaymentAccount::where(['id' => $id, 'trader_id' => $trader->id])->firstOrFail();
        $trader_account->volume = 0;
        if ($trader_account->save()) {
            return redirect()->back()->with(['alert-type' => 'success', 'message' => __('Reseted')]);
        }
        return redirect()->back()->with(['alert-type' => 'error', 'message' => __('Error')]);
    }
    public function gettypes(Request $request)
    {
        $type_ids = TraderPaymentSystemTypeRelation::where(
            [
                'trader_payment_system_id' => $request->input('id')
            ]
        )->pluck('payment_system_type_id');

        return PaymentSystemType::whereIn('id', $type_ids)->get();
    }

    public function getPaySystems(Request $request)
    {
        $payment_systems = TraderFiat::where('id', $request->input('id'))->first();
        return $payment_systems->payment_systems ?? [];
    }

    protected function addCardNumberRule($rules, $request)
    {
        $pst = PaymentSystemType::find((int) $request->input('payment_system_type_id'));

        switch ($pst->obj_name) {
            case 'card_number': {
                    $rules['card_number'] = ['required', 'string', function ($attribute, $value, $fail) {
                        if (!preg_match('/^\d{13,19}$/', $value)) {
                            $fail(':attribute format is invalid');
                        }

                        $luhn = new LuhnAlgorithm();
                        if (!$luhn->isValid($value)) {
                            $fail(':attribute control sum not match');
                        }
                    }];
                    break;
                }
            case 'iban': {
                    $rules['card_number'] = 'required|string|min:15|max:40';
                    break;
                }
            case 'upi': {
                    $rules['card_number'] = 'required|string';
                    break;
                }
            case 'qiwi': {
                    $rules['card_number'] = 'required|string|min:11|max:15';
                    break;
                }
        }

        return $rules;
    }
}

<?php

return [
    'on' => env('TELEGRAM_ON', 0),
    'bots' => [
        'mybot' => [
            'username'            => env('TELEGRAM_BOT_NAME', ''),
            'token'               => env('TELEGRAM_BOT_TOKEN', ''),
            'certificate_path'    => env('TELEGRAM_CERTIFICATE_PATH', ''),
            'webhook_url'         => env('TELEGRAM_WEBHOOK_URL', ''),
            'commands'            => [],
        ]
    ]
];

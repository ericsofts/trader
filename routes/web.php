<?php

use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TraderController;
use App\Http\Controllers\TraderAdController;
use App\Http\Controllers\TraderDealController;
use App\Http\Controllers\TwoFAController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\TraderPaymentAccountController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

// Locale
Route::get('setlocale/{locale}', function ($locale) {
    if (in_array($locale, config()->get('app.locales'))) {
        session()->put('locale', $locale);
    }
    return redirect()->back()->cookie('locale', $locale, 60*24*360);
});

Route::middleware(['auth', 'verified'])->group(function() {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/dashboard/balance', [DashboardController::class, 'balance'])->name('dashboard.balance');
    Route::get('/dashboard/deals', [DashboardController::class, 'deals'])->name('dashboard.deals');
    Route::get('/dashboard/psinfo', [DashboardController::class, 'psInfo'])->name('dashboard.psinfo');
    Route::get('/dashboard/dealstat', [DashboardController::class, 'dealStat'])->name('dashboard.dealstat');

    Route::get('/trader/ad/available-accounts', [TraderAdController::class, 'availableAccounts'])->name('trader.ad.available_accounts');
    Route::post('/trader/ad/create', [TraderAdController::class, 'create'])->name('trader_ad.create');
    Route::get('/trader/ad/edit/{id}', [TraderAdController::class, 'edit'])->name('trader_ad.edit');
    Route::patch('/trader/ad/update/{id}', [TraderAdController::class, 'update'])->name('trader_ad.update');
    Route::get('/trader/ad/rates', [TraderAdController::class, 'rates'])->name('trader_ad.rates');
    Route::get('/trader/ad/{type}', [TraderAdController::class, 'index'])->name('trader_ad.index');
    Route::get('/trader/ad/new/{type}', [TraderAdController::class, 'new'])->name('trader_ad.new');

    Route::post('/trader/ad/disable/{id}', [TraderAdController::class, 'disable'])->name('trader.ad.disable');
    Route::post('/trader/ad/enable/{id}', [TraderAdController::class, 'enable'])->name('trader.ad.enable');
    Route::post('/trader/ad/disableall', [TraderAdController::class, 'disableall'])->name('trader.ad.disableall');
    Route::post('/trader/ad/enableall', [TraderAdController::class, 'enableall'])->name('trader.ad.enableall');

    Route::get('/trader/deal/index/{type}', [TraderDealController::class, 'index'])->name('trader_deal.index');
    Route::get('/trader/deal/history/{id}', [TraderDealController::class, 'history'])->name('trader_deal.history');
    Route::post('/trader/deal/message/send/{id}', [TraderDealController::class, 'messageSend'])->name('trader_deal.message.send');
    Route::get('/trader/deal/detail/{id}', [TraderDealController::class, 'detail'])->name('trader_deal.detail');
    Route::get('/trader/deal/detail_info/{id}', [TraderDealController::class, 'detailInfo'])->name('trader_deal.detail_info');
    Route::get('/trader/deal/messages/{id}', [TraderDealController::class, 'messages'])->name('trader_deal.messages');
    Route::post('/trader/deal/status/{id}', [TraderDealController::class, 'status'])->name('trader_deal.status');
    Route::get('/trader/deal/message/attachment/{id}', [TraderDealController::class, 'attachment'])->name('trader_deal.message.attachment');

    Route::get('/trader/payment-system-accounts', [TraderPaymentAccountController::class, 'index'])->name('trader.payment-system-accounts');
    Route::post('/trader/payment-system-types', [TraderPaymentAccountController::class, 'get'])->name('trader.payment-system-types');
    Route::post('/trader/payment-system-delete/{id}', [TraderPaymentAccountController::class, 'delete'])->name('trader.payment-system-accounts.delete');
    Route::post('/trader/payment-system-accounts/disable/{id}', [TraderPaymentAccountController::class, 'disable'])->name('trader.payment-system-accounts.disable');
    Route::post('/trader/payment-system-accounts/enable/{id}', [TraderPaymentAccountController::class, 'enable'])->name('trader.payment-system-accounts.enable');
    Route::post('/trader/payment-system-accounts/reset/{id}', [TraderPaymentAccountController::class, 'reset'])->name('trader.payment-system-accounts.reset');
    Route::get('/trader/payment-system-accounts/edit/{id}', [TraderPaymentAccountController::class, 'edit'])->name('trader.payment-system-accounts.edit');
    Route::post('/trader/payment-system-accounts/update/{id}', [TraderPaymentAccountController::class, 'update'])->name('trader.payment-system-accounts.update');
    Route::post('/trader/payment-system-accounts/add', [TraderPaymentAccountController::class, 'add'])->name('trader.payment-system-accounts.add');
    Route::get('/trader/payment-system-accounts/new', [TraderPaymentAccountController::class, 'new'])->name('trader.payment-system-accounts.new');
    Route::post('/trader/payment-system-accounts/gettypes', [TraderPaymentAccountController::class, 'gettypes'])->name('trader.payment-system-accounts.gettypes');
    Route::post('/trader/payment-system-accounts/getpaysystems', [TraderPaymentAccountController::class, 'getPaySystems'])->name('trader.payment-system-accounts.getpaysystems');

    Route::get('/profile', [TraderController::class, 'profile'])->name('trader.profile');
    Route::patch('/profile/save', [TraderController::class, 'profileSave'])->name('trader.profile.save');
    //Route::get('/balances', [TraderController::class, 'balances'])->name('trader.balances');
    Route::get('/trader/address_qr/{address}', [TraderController::class, 'address_qr'])->name('trader.address_qr');
    Route::post('/trader/get_address', [TraderController::class, 'get_address'])->name('trader.get_address');

    Route::get('2fa', [TwoFAController::class, 'index'])->name('2fa.index');
    Route::get('2fa/status', [TwoFAController::class, 'status'])->name('2fa.status');
    Route::post('2fa/configure', [TwoFAController::class, 'configure'])->name('2fa.configure');
    Route::post('2fa/disable', [TwoFAController::class, 'disable'])->name('2fa.disable');
    Route::post('2fa/generate_new', [TwoFAController::class, 'generateNew'])->name('2fa.generate_new');
});

Route::get('/login/verify-2fa', [AuthenticatedSessionController::class, 'showVerify2fa'])->name('login.verify2fa');
Route::post('/login/verify-2fa', [AuthenticatedSessionController::class, 'verify2fa'])->name('login.verify2fa');

require __DIR__.'/auth.php';

import Vue from 'vue'

if (process.env.MIX_ENV_MODE === 'production') {
    Vue.config.devtools = false;
    Vue.config.debug = false;
    Vue.config.silent = true;
}

Vue.component('Balance', require('./components/Balance.vue').default);
Vue.component('Deals', require('./components/Deals.vue').default);
Vue.component('Psinfo', require('./components/Psinfo.vue').default);
Vue.component('Dealmessages', require('./components/Dealmessages.vue').default);
Vue.component('Dealdetail', require('./components/Dealdetail.vue').default);
Vue.component('Dealstat', require('./components/Dealstat.vue').default);

var App = new Vue({
    el: '#app'
});

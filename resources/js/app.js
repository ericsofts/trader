import { Modal } from 'bootstrap';
import $ from 'jquery';
import 'daterangepicker'
import moment from 'moment';
import Swal from 'sweetalert2'

//https://github.com/CodeSeven/toastr
window.toastr = require('toastr');
window.$ = window.jQuery = $;
window.Swal = Swal;
require('select2')

var fiat = null;
var available_payment_types = [];

$(function () {
    $('.select2').select2({
        width: 'resolve',
        theme: 'bootstrap-5',
    })

    $('.datepicker').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        autoApply: true,
        timePicker: true,
        timePicker24Hour: true,
        minYear: 1901,
        maxYear: parseInt(moment().add(1, 'years').format('YYYY'), 10),
        autoUpdateInput: false,
        locale: {
            format: 'YYYY-MM-DD HH:mm',
        }
    }, function (start, end, label) {

    });

    $('.datepicker').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD HH:mm'));
    });

    $('.datepicker').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
    var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            container: 'swal2-grow',
            content: 'fw-bold',
            confirmButton: 'btn btn-grow',
            cancelButton: 'btn btn-link'
        },
        buttonsStyling: false
    })

    $(document).on("click", ".btn-confirm-code", function (e) {
        e.preventDefault();

        var form = $(this).closest('form');

        var placeholder = tt['Code'];
        var html = tt['Confirm Code'].replace(/%ACTION%/, $(this).html().trim());
        var inputType = 'text';

        var addInputs = [];
        var options = '';

        swalWithBootstrapButtons.fire({
            html: html,
            input: inputType,
            inputPlaceholder: placeholder,
            inputValidator: (value) => {
            },
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: tt['Submit'],
            cancelButtonText: tt['Cancel']
        }).then((result) => {
            if (result.isConfirmed) {
                form.prepend('<input type="hidden" name="code" value="' + $('#swal2-content + .swal2-input').val() + '">');
                if(addInputs.length){
                    form.prepend('<input type="hidden" name="trader_payment_account_id" value="' + $('#payment_account_select').val() + '">');
                }
                form.submit();
            }
        })
        if ($(this).attr('data-inputs')) {
            addInputs = JSON.parse($(this).attr('data-inputs'));
            $('.swal2-content').append('<label>' + tt['Payment Accounts'] + '</label><select class="swal2-input" name="trader_payment_account_id" id="payment_account_select"></select>');
            if(addInputs.length){
                addInputs.forEach(function (el) {
                    var name = el.name ?? el.card_number;
                    options += '<option value="' + el.id + '">' + name + '</option>'
                });
            }

            $('#payment_account_select').html(options);
        }
        return false;
    });

    $(document).on("click", ".btn-confirm-default", function (e) {
        e.preventDefault();
        var form = $(this).closest('form');
        var text = $(this).attr('data-text') || 'CONFIRM';
        var html = tt['Confirm Text'].replace(/%TEXT%/, text);

        swalWithBootstrapButtons.fire({
            html: html,
            input: 'text',
            inputPlaceholder: text,
            inputValidator: (value) => {
                if (value !== text) {
                    return ''
                }
            },
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Submit',
            cancelButtonText: 'Cancel'
        }).then((result) => {
            if (result.isConfirmed) {
                form.submit();
            }
        })
        return false;
    });

    var taf = $('.trader-ad-form');
    if (taf) {
        taf.find('#fiat_id').on('change', function () {
            fiats.forEach(function (el) {
                if (el.id != $('#fiat_id').val()) {
                    return;
                }

                fiat = el;

                $('#payment_system_id').find('option:not([value=""])').remove();
                var buf = '';

                el.payment_systems.forEach(function (el) {
                    buf += '<option value="' + el.id + '">' + el.name + '</option>';
                });
                $('#payment_system_id').html(
                    $('#payment_system_id').html() + buf
                );

                $('#payment_system_id').find('option[value="' + $('#payment_system_id').attr('value') + '"]').attr('selected', true);
            });
        });
        taf.find('#fiat_id').trigger('change');

        taf.find('#payment_system_id').on('change', function () {
            taf.find('#payment_system_type_id option').hide();

            if (fiat && fiat.id == taf.find('#fiat_id').val()) {
                fiat.payment_systems.forEach(function (payment_system) {
                    if (payment_system.id == taf.find('#payment_system_id').val()) {
                        available_payment_types = payment_system.available_payment_types;

                        payment_system.available_payment_types.forEach(function (available_payment_type) {
                            taf.find('#payment_system_type_id [value="' + available_payment_type.id + '"]').show();
                        });

                    }
                });
            }

            if (!$('#payment_system_type_id option:visible').length) {
                taf.find('#payment_system_type_id').val('');
            }

            taf.find('#payment_system_type_id').trigger('change');
        });
        taf.find('#payment_system_id').trigger('change');

        taf.find('#payment_system_type_id').on('change', function () {
            $.get(urls.availableAccounts,
                {
                    payment_system_id: taf.find('#payment_system_id').val(),
                    payment_system_type_id: taf.find('#payment_system_type_id').val(),
                    trader_ad_id: AD_ID
                }, function (data) {
                    var result = '';

                    if (data.length) {
                        result += '<div class="card p-2">';
                        data.forEach(function (el) {
                            result += '<div class="mb-1"><label><input type="checkbox" name="payment_accounts_ids[]" ' + (el.checked ? 'checked' : '') + ' value="' + el.id + '"> ' + (el.name || el.card_number) + '</label></div>';
                        });
                        result += '</div>';
                    }

                    $('#card_number').html(result);
                });
        });
        taf.find('#payment_system_type_id').trigger('change');

        taf.find('#type').on('change', function () {
            if (parseInt($(this).val()) == TYPE_SELL) {
                $('#card_number_toggler').show();
                $('.type-buy').hide();
                $('.type-sell').show();
            }
            else {
                $('#card_number_toggler').hide();
                $('.type-buy').show();
                $('.type-sell').hide();
            }
        });
        taf.find('#type').trigger('change');

        taf.find('#fiat_id').on('change', function () {
            $('.fiat-asset').html($(this).find('option:selected').data('asset'));
        });
        taf.find('#fiat_id').trigger('change');

        taf.find('#currency_id, #fiat_id, #rate_type, #rate, #rate_source_id').on('change', function () {
            if ($('#rate_type').val() != RATE_PERCENTAGE) {
                return false;
            }

            var rate_source_id = $('#rate_source_id').val();
            var currency_id = $('#currency_id').val();
            var fiat_id = $('#fiat_id').val();

            if (!rate_source_id || !currency_id || !fiat_id) {
                return false;
            }

            $.get(RATES_URL, {
                rate_source_id: rate_source_id,
                currency_id: currency_id,
                fiat_id: fiat_id
            }, function (data) {
                var drate = parseFloat(data);
                if (!drate) {
                    return false;
                }

                var perc = $('#rate').val() / 100;

                var placeholder = ''
                    + $('#currency_id :selected').data('asset')
                    + '/'
                    + $('#fiat_id :selected').data('asset')

                var value = ' = ' + drate.toPrecision(4) + ' ' + $('#fiat_id :selected').data('asset');
                taf.find('#rate_from_source').html(placeholder + value);

                drate = drate * (1 + perc);

                placeholder += (perc >= 0 ? '+' : '') + perc * 100 + '%';
                value = ' = ' + drate.toPrecision(4) + ' ' + $('#fiat_id :selected').data('asset');

                taf.find('#rate_dynamic').html(placeholder + value);
            });
        });

        taf.find('#rate_type').on('change', function () {
            if (parseInt($(this).val()) === RATE_PERCENTAGE) {
                $('#temp_rate_toggler').show();
                $('#rate_stop_toggler').show();
                $('#rate_source_id_toggler').show();
                $('.type-percentage').show();
                $('.type-fixed').hide();
            }
            else {
                $('#temp_rate_toggler').hide();
                $('#rate_stop_toggler').hide();
                $('#rate_source_id_toggler').hide();
                $('.type-percentage').hide();
                $('.type-fixed').show();
            }
        });
        taf.find('#rate_type').trigger('change');
    }

    $(document).on('change', '.enable-2fa', function (event) {
        event.preventDefault();
        var that = this;
        if (this.checked) {
            $('.block-form-2fa').show();
            $('#enable_2fa').val(1);
        } else {
            $(that).attr('disabled', true);
            $.ajax({
                url: '/2fa/status',
                type: 'GET'
            }).done(function (data) {
                if (data.status) {
                    $(that).prop('checked', true);
                    var modal = new Modal(document.getElementById('modal-2fa-disable'))
                    modal.show()
                } else {
                    $('.block-form-2fa').hide();
                }
            }).always(function () {
                $(that).attr('disabled', false);
            })
        }
        return false;
    })

    $(document).on('submit', 'form#disable-2fa', function (event) {
        event.preventDefault();
        var form = $(this).closest('.modal-content').find('form');
        var that = this;
        $(that).attr('disabled', true);
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: form.serialize()
        }).done(function (data) {
            if (data.status) {
                toastr.success(data.message);
                window.location.reload(true);
            } else {
                toastr.error(data.message);
                $.each(data.errors, function (index, value) {
                    form.find('#' + index).addClass('is-invalid');
                    form.find('#' + index).siblings('.invalid-feedback').text(value[0]);
                });
            }
        }).always(function () {
            $(that).attr('disabled', false);
            form.find("input[type=text]").val("");
        })
        return false;
    });

    $(document).on('click', '.btn-regenerate-2fa', function (event) {
        event.preventDefault();
        var modal = new Modal(document.getElementById('modal-2fa-generate'))
        modal.show()
        return false;
    })

    $(document).on('submit', 'form#2fa-generate', function (event) {
        event.preventDefault();
        var form = $(this).closest('.modal-content').find('form');
        var that = this;
        $(that).attr('disabled', true);
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: form.serialize()
        }).done(function (data) {
            if (data.status) {
                toastr.success(data.message);
                window.location.reload(true);
            } else {
                toastr.error(data.message);
                $.each(data.errors, function (index, value) {
                    form.find('#' + index).addClass('is-invalid');
                    form.find('#' + index).siblings('.invalid-feedback').text(value[0]);
                });
            }
        }).always(function () {
            $(that).attr('disabled', false);
            form.find("input[type=text]").val("");
        })
        return false;
    });
});

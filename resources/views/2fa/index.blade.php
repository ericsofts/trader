@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Security options')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Security options') }}
        </h1>
    </div>
    <div class="content-wrapper content-wrapper-2fa">
        <div class="content p-2 py-4 content-profile">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="content-title">{{__('Two-Factor Auth')}}</h1>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-top mb-3">
                            <div class="card-body">
                                <h5>{{__('What is Two-Factor Authentication (2FA) and why do you need it?')}}</h5>
                                <p>{{__('Enabling Two-Factor Authentication provides higher level of security when accessing your personal account on this website.')}}</p>
                                <p>{{__('Two-Factor Authentication will require an additional passcode after entering login and password, before granting access to personal account.')}}</p>
                                <p>{{__('Two-Factor Authentication passcodes are sent through mobile 2FA applications generating tokens (keys), email letters which are sent after entering your login and password, or can be taken from a previously generated list of passcodes.')}}</p>
                                <p>{{__('The settings provided on this page allow you to configure which of these authentication code providers will be enabled for your personal account.')}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-top mb-3">
                            <div class="card-body">
                                <h5>{{__('Turning on 2fa instruction:')}}</h5>
                                <p class="fw-bold">{{__('01. Enable 2FA')}}</p>
                                <p class="fw-bold">{{__('02. Install any 2FA application to your choice:')}}</p>
                                <p class="mb-0">{{__('For Android devices')}}:</p>
                                <ul class="list-unstyled">
                                    <li><a href="https://play.google.com/store/apps/details?id=com.authy.authy"
                                           target="_blank">> Authy</a></li>
                                    <li>
                                        <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2"
                                           target="_blank">> Google Authenticator</a></li>
                                </ul>
                                <p class="mb-0">{{__('For iOS devices')}}:</p>
                                <ul class="list-unstyled">
                                    <li><a href="https://itunes.apple.com/us/app/authy/id494168017"
                                           target="_blank">> Authy</a></li>
                                    <li>
                                        <a href="https://apps.apple.com/ru/app/google-authenticator/id388497605"
                                           target="_blank">> Google Authenticator</a></li>
                                </ul>

                                <p>{{__('To generate a time-based one-time passcode, it is necessary to install and set  up the application for generating tokens (keys) on your Android or iOS mobile device.')}}</p>
                                <p class="fw-bold">{{__('03. Scan the QR code')}}</p>
                                <p>{{__('Open the app and scan QR code on this page.')}}</p>
                                <p class="fw-bold">{{__('04. Enter the passcode from the application when logging in to your Personal Account.')}}</p>
                                <ol>
                                    <li>{{__('Log out and log in to your Personal Account again.')}}</li>
                                    <li>{{__('Enter Login and Password.')}}</li>
                                    <li>{{__('In the next field, enter the passcode from the application.')}}</li>
                                </ol>
                                <p>{{__('Thank you for using the service!')}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-empty">
                            <div class="card mb-3">
                                <div class="card-body">
                                    <form method="post" action="{{ route('2fa.disable') }}">
                                        @method('POST')
                                        @csrf
                                        <div class="form-check">
                                            <div class="form-check form-switch">
                                                <input type="hidden" value="0" name="enable2fa">
                                                <input class="form-check-input enable-2fa"
                                                       type="checkbox"
                                                       id="enable2fa"
                                                       name="enable2fa"
                                                       value="1"
                                                    {{old('enable_2fa', $enable2fa) ? 'checked' : ''}}
                                                >
                                                <label class="form-check-label"
                                                       for="enable2fa"><strong>{{__('Enable 2FA')}}</strong></label>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="card block-form-2fa"
                                @if(!old('enable_2fa', $enable2fa)) style="display: none;" @endif>
                                <div class="card-body">
                                    @if (!$enable2fa)
                                        <p class="card-sub-title">{{__('Scan QR-code from 2FA app on your mobile device:')}}</p>

                                        <img class="img-fluid" src="{{$qr}}">
                                        <hr/>

                                        <p>{{__('In case you lost your phone or unable to scan the QR-code, enter the manual secret code in the token generating app.')}}</p>
                                        <div class="mb-3 form-group">
                                            <label for="Secret">{{__('Secret')}}:</label> {{$secret}}
                                        </div>
                                    @endif

                                    @if($enable2fa)
                                        <div class="mb-3">
                                            <button type="button"
                                                class="btn btn-grow btn-regenerate-2fa">{{__('Generate new secret')}}</button>
                                        </div>
                                    @else
                                        <form method="post" id="configure-2fa"
                                              action="{{ route('2fa.configure') }}">
                                            @method('POST')
                                            @csrf
                                            <input type="hidden" name="secret" value="{{$secret}}">
                                            <input type="hidden" name="enable_2fa" id="enable_2fa"
                                                   value="{{old('enable_2fa', $enable2fa) ? 1 : 0}}"
                                            >
                                            <div class="mb-3 form-group">
                                                <label class="form-label" for="code">{{__('Code')}}</label>
                                                <input
                                                    name="code"
                                                    type="text"
                                                    class="form-control @error('code') is-invalid @enderror"
                                                    id="code"
                                                    placeholder="{{__('Code')}}"
                                                    value=""
                                                >
                                                @error('code')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <button type="submit" class="btn btn-grow">{{__('Confirm')}}</button>
                                            </div>
                                        </form>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-2fa-disable" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" id="disable-2fa" action="{{ route('2fa.disable') }}" class="form-horizontal">
                    @method('POST')
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalLabel">{{__('Two-factor authentication')}}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-sm-8 col-form-label-qr mt-3">
                                <label for="disable_code"
                                       class="">{{ __('Please enter the code from the mobile Two-factor Authentication application (2FA)')}}</label>
                            </div>
                            <div class="col-sm-4 mx-auto">
                                <div class="mt-3">
                                    <input id="disable_code" type="text"
                                           placeholder="{{ __('Code') }}"
                                           class="form-control"
                                           name="disable_code"
                                           required
                                           autofocus
                                    >
                                    <span class="invalid-feedback" role="alert"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-link" data-bs-dismiss="modal">{{__('Cancel')}}</button>
                        <button type="submit" class="btn btn-grow">{{__('Confirm')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-2fa-generate" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" id="2fa-generate" action="{{ route('2fa.generate_new') }}" class="form-horizontal">
                    @method('POST')
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalLabel">{{__('Two-factor authentication')}}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-sm-8 col-form-label-qr mt-3">
                                <label for="generate_code"
                                       class="">{{ __('Please enter the code from the mobile Two-factor Authentication application (2FA)')}}</label>
                            </div>
                            <div class="col-sm-4 mx-auto mt-3">
                                <input id="generate_code" type="text"
                                       placeholder="{{ __('Code') }}"
                                       class="form-control"
                                       name="generate_code"
                                       required
                                       autofocus
                                >
                                <span class="invalid-feedback" role="alert"></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-link" data-bs-dismiss="modal">{{__('Cancel')}}</button>
                        <button type="submit" class="btn btn-grow">{{__('Confirm')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

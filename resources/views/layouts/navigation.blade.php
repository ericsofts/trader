<?php
    use App\Models\TraderAd;
    use App\Models\TraderDeal;
    $adTypes = TraderAd::getRouteTypes();
    $dealTypes = TraderDeal::getRouteTypes();
?>

<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse sidebar-menu">
    <div class="position-sticky pt-3">
        <ul class="nav flex-column">
            <li class="nav-item">
                <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mb-1 text-muted">
                    {{ Auth::user()->name }}
                </h6>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('dashboard')) active @endif"
                    href="{{route('dashboard')}}">
                    <i class="bi bi-house"></i> {{__('Dashboard')}}
                </a>
            </li>
            <ul class="nav flex-column mb-2">
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('trader_deal.index') && isset($type) && $type == TraderDeal::TYPE_BUY) active @endif"
                       href="{{route('trader_deal.index', ['type' => $dealTypes[TraderDeal::TYPE_BUY]])}}">
                        <i class="bi bi-arrow-down-left-circle"></i> {{__('Buying Deals')}}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('trader_deal.index') && isset($type) && $type == TraderDeal::TYPE_SELL) active @endif"
                       href="{{route('trader_deal.index', ['type' => $dealTypes[TraderDeal::TYPE_SELL]])}}">
                        <i class="bi bi-arrow-down-left-circle"></i> {{__('Selling Deals')}}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('trader_ad.index') && isset($type) && $type == TraderAd::TYPE_BUY) active @endif"
                       href="{{route('trader_ad.index', ['type' => $adTypes[TraderAd::TYPE_BUY]])}}">
                        <i class="bi bi-arrow-up-right-circle"></i> {{__('Buying Ads')}}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if(request()->routeIs('trader_ad.index') && isset($type) && $type == TraderAd::TYPE_SELL) active @endif"
                       href="{{route('trader_ad.index', ['type' => $adTypes[TraderAd::TYPE_SELL]])}}">
                        <i class="bi bi-arrow-up-right-circle"></i> {{__('Selling Ads')}}
                    </a>
                </li>
            </ul>
            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                <span>{{__('Settings')}}</span> <i class="bi bi-gear"></i>
            </h6>
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('trader.profile')) active @endif" href="{{route('trader.profile')}}">
                    <i class="bi bi-person-fill"></i>
                    {{__('Profile')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('trader.payment-system-accounts')) active @endif" href="{{route('trader.payment-system-accounts')}}">
                    <i class="bi-cash-stack"></i>
                    {{__('Payment Accounts')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('2fa.index')) active @endif" href="{{route('2fa.index')}}">
                    <i class="bi bi-shield-lock"></i>
                    {{__('Security options')}}
                </a>
            </li>
            {{--<li class="nav-item">
                <a class="nav-link @if(request()->routeIs('trader.balances')) active @endif"
                    href="{{route('trader.balances')}}">
                    <i class="bi bi-person"></i> {{__('Balances')}}
                </a>
            </li>--}}
        </ul>
    </div>
</nav>

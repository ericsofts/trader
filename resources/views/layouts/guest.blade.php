<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="@yield('html-class')">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ mix('js/app.js') }}"></script>
    </head>
    <body class="bg-black flex-column @yield('body-class')">
    <header class="navbar flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3 text-white" href="/">{{ config('app.name', 'Laravel') }}</a>
        <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="d-flex justify-content-end">
            <ul class="nav px-3 nav-locales">
                @foreach (config()->get('app.locales') as $locale)
                    <li class="nav-item{{ $locale == app()->getLocale() ? " active" : "" }}">
                        <a class="nav-link"
                           href="/setlocale/{{ $locale }}"><img class="img-flag"
                                                                src="{{ asset("images/flags/$locale.png") }}"></a>
                    </li>
                @endforeach
            </ul>
        </div>
    </header>
        @yield('content')
        @if(session()->has('message'))
            <script>
                window.onload = function(e){
                    toastr.options.progressBar = true;
                    var type = "{{ session()->get('alert-type', 'info') }}";
                    switch(type){
                        case 'info':
                            toastr.info("{{ session()->get('message') }}");
                            break;

                        case 'warning':
                            toastr.warning("{{ session()->get('message') }}");
                            break;

                        case 'success':
                            toastr.success("{{ session()->get('message') }}");
                            break;

                        case 'error':
                            toastr.error("{{ session()->get('message') }}");
                            break;
                    }
                }
            </script>
        @endif
    </body>
</html>

@extends('layouts.app')
@section('content')
<div class="page-header">
    <h1>
        {{__('Welcome')}}, {{request()->user()->name}}
    </h1>
</div>

<div class="row gx-0">
    <div class="me-md-3 col-md-3 col-lg-2">
        <Dealstat />
    </div>
    <div class="me-md-3 col-md-6 col-lg-4">
        <Balance />
    </div>
</div>

<div class="page-header">
    <h1>
        {{ __('Active Deals') }}
    </h1>
</div>

<div class="table-responsive">
    <Deals />
</div>
@endsection
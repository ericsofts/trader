@extends('layouts.app')
@section('content')

<nav aria-label="breadcrumb" class="pt-3">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
        <li class="breadcrumb-item active">{{__('Profile')}}</li>
    </ol>
</nav>

<div class="page-header">
    <h1>
        {{ __('Profile') }}
    </h1>
</div>

<form method="POST" action="{{ route('trader.profile.save') }}" class="row mb-3">
    @csrf
    @method('PATCH')

    <div class="col-md-7">
        <div class="col-lg-6">
            <div class="row mb-3">
                <p>{{__('Telegram Bot')}}: <a target="_blank" href="https://t.me/{{config('telegram.bots.mybot.username')}}">&#64;{{config('telegram.bots.mybot.username')}}</a></p>

                <label class="col-sm-12 col-form-label">{{__('Telegram ID`s for notifications')}}:</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('telegram') is-invalid @enderror" value="{{ old('telegram', $trader->telegram) }}" name="telegram" id="telegram" placeholder="{{__('Comma separated')}}">

                    @error('telegram')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-12 col-form-label">{{__('Timezone')}}:</label>
                <div class="col-sm-12">
                    <select name="timezone" class="form-control">
                        <option value="">{{__('Choose')}}</option>
                        @foreach($timezoneList as $value)
                        <option value="{{$value}}" @if(old('timezone', $timezone)==$value) selected @endif>{{$value}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-12 col-form-label">{{__('Default Currency')}}:</label>
                <div class="col-sm-12">
                    <select name="default_currency" class="form-control">
                        <option value="">{{__('Not defined')}}</option>
                        @foreach($currencyList as $unit)
                        <option value="{{$unit->id}}" @if(old('default_currency', $default_currency)==$unit->id) selected @endif>{{$unit->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="page-header">
                <h1>
                    {{ __('Custom Status Code')}}
                </h1>
            </div>
            {{-- <div class="row">--}}
            {{-- <div class="col-md-12">--}}
            {{-- <div class="row">--}}
            {{-- <label class="col-sm-12 col-form-label">{{__('Custom Status Code')}}:</label>--}}
            {{-- </div>--}}
            {{-- </div>--}}
            {{-- </div>--}}
            @foreach($avalableStatusesData as $key=>$avalableStatus)
            <div class="row mb-3">
                <div class="col-sm-4">
                    <div class="row">
                        <label class="col-form-label">{{$avalableStatus['label']}}:</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="col-sm-12">
                            <input type="text" class="form-control @error('code.'.$avalableStatus['code']) is-invalid @enderror" value="{{ old('code.'.$avalableStatus['code'], $avalableStatus['value'] ?? '') }}" name="code[{{$avalableStatus['code']}}]" id="code_{{$avalableStatus['code']}}" placeholder="{{__('Code')}}">

                            @error('code.'.$avalableStatus['code'])
                            <div class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

        <div class="col-md-12">
            <div class="row">
                <div class="col">
                    <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
                </div>
            </div>
        </div>
</form>

@endsection
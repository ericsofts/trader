@extends('layouts.app')
@section('content')

<nav aria-label="breadcrumb" class="pt-3">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
        <li class="breadcrumb-item active">{{__('Balances')}}</li>
    </ol>
</nav>

<div class="page-header">
    <h1>
        {{ __('Balances') }}
    </h1>
</div>

<div class="row gx-0">
    @foreach ($trader_currency as $unit)
        <div class="card p-3 me-md-3" style="width: auto;">
            <h5>{{$unit->name}}</h5>
            <p>{{__('Balance')}}: <strong>{{$unit->balance_amount}} {{explode('_', $unit->asset)[0]}}</strong></p>

            @if ($unit->currency_address)
                <img class="border mb-3" src="{{route('trader.address_qr', $unit->currency_address->address)}}" width="200">
                <div style="font-size: 11px;">{{$unit->currency_address->address}}</div>
            @endif

            @if ($unit->can_get_address)
                <form class="mt-3" action="{{route('trader.get_address')}}" method="post">
                    @csrf
                    <input type="hidden" name="currency_id" value="{{$unit->id}}">
                    @if ($unit->currency_address)
                        <button class="btn btn-grow btn-confirm-default">{{__('Get New Address')}}</button>
                    @else
                        <button class="btn btn-grow btn-confirm-default">{{__('Get Address')}}</button>
                    @endif
                </form>
            @endif
        </div>
    @endforeach
</div>

@endsection
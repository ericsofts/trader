@extends('layouts.app')

@section('content')
    <ul class="nav nav-tabs mt-3" role="tablist">
        <li class="nav-item" role="presentation">
            <a href="{{route('trader_deal.detail', ['id' => $id])}}" class="nav-link active" role="tab">{{__('Deal Details')}}</a>
        </li>
        <li class="nav-item" role="presentation">
            <a href="{{route('trader_deal.history', ['id' => $id])}}" class="nav-link" role="tab">{{__('Deal History')}}</a>
        </li>
    </ul>

    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('trader_deal.index', ['type' => $route_types[$deal->type]])}}">{{__(['Selling Deals', 'Buying Deals'][$deal->type])}}</a></li>
            <li class="breadcrumb-item active">{{__('Deal Details') . " #$deal->id"}}</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-lg-6 colDetails">
            <div class="card p-3 mb-3">
                <div class="page-header">
                    <h1>
                        {{ __('Deal Details') }}
                    </h1>
                </div>
                <Dealdetail/>
            </div>
        </div>
        <div class="col-lg-6 colMessages">
            <div class="card p-3 mb-3">
                <div class="page-header">
                    <h1>
                        {{ __('Deal Messages') }}
                    </h1>
                </div>
                <div class="row">
                    <div class="col-12 form-message">
                        <form method="post" action="{{ route('trader_deal.message.send', ['id' => $id]) }}" enctype="multipart/form-data">
                            @method('POST')
                            @csrf
                            <input type="hidden" name="id" value="{{$id}}">
                            <div class="mb-3">
                                <label for="message" class="form-label">{{__('Message')}}</label>
                                <textarea class="form-control @error('message') is-invalid @enderror" id="message"
                                            name="message" rows="3"></textarea>
                                @error('message')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="document" class="form-label">{{__('Image')}}</label>
                                <input class="form-control" accept="image/png,image/jpeg" type="file" id="document" name="document">
                            </div>
                            <div class="my-3">
                                <button type="submit" class="btn btn-grow">{{__('Send')}}</button>
                            </div>
                        </form>
                    </div>
                </div>

                <Dealmessages/>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.app')
@section('content')
    <ul class="nav nav-tabs mt-3" role="tablist">
        <li class="nav-item" role="presentation">
            <a href="{{route('trader_deal.detail', ['id' => $id])}}" class="nav-link" role="tab">{{__('Deal Details')}}</a>
        </li>
        <li class="nav-item" role="presentation">
            <a href="{{route('trader_deal.history', ['id' => $id])}}" class="nav-link active" role="tab">{{__('Deal History')}}</a>
        </li>
    </ul>

    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('trader_deal.index', ['type' => $route_types[$deal->type]])}}">{{__('Deals')}}</a></li>
            <li class="breadcrumb-item active">{{__('Deal History') . " #$deal->id"}}</li>
        </ol>
    </nav>

    <div class="page-header">
        <h1>
            {{ __('Deal History') }}
        </h1>
    </div>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">{{__('ID')}}</th>
            <th scope="col">{{__('Description')}}</th>
            <th scope="col">{{__('Status')}}</th>
            <th scope="col">{{__('Customer Type')}}</th>
            <th scope="col">{{__('Customer Name')}}</th>
            <th scope="col">{{__('Created At')}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($dealHistory as $key => $item)
            <tr>
                <td>{{$item->deal_id}}</td>
                <td>{{$item->description}}</td>
                <td>{{__('status_' . $item->status)}}</td>
                <td>{{__($customerType[$item->customer_type]['label'] ?? '')}}</td>
                <td>{{$item->customer_name ?? ''}}</td>
                <td>{{datetimeFormat($item->created_at)}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="float-end">{{ $dealHistory->links() }}</div>
@endsection

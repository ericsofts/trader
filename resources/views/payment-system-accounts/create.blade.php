@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('trader.payment-system-accounts')}}">{{__('Payment Accounts')}}</a></li>
            <li class="breadcrumb-item active">{{__('New Payment Account')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('New Payment Account') }}
        </h1>
    </div>
    <form method="POST" action="{{ route('trader.payment-system-accounts.add') }}" class="row mb-3">
        @csrf
        @method('POST')
        <div class="col-lg-3">
            <div class="row mb-3">
                <label class="col-sm-12 col-form-label">{{ __('Name') }}</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="name" value="{{old('name')}}">
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="row mb-3">
                <label class="col-sm-12 col-form-label">{{ __('Fiat') }}</label>
                <div class="col-sm-12">
                    <select class="form-control @error('trader_fiat_id') is-invalid @enderror" name="trader_fiat_id" id="trader_fiat_id">
                        <option value="">{{ __('Choose') }}</option>
                        @if($fiats)
                            @foreach($fiats as $fiat)
                                <option value="{{ $fiat->id }}" @if(old('trader_fiat_id') == $fiat->id) selected @endif>{{ $fiat->name }}</option>
                            @endforeach
                        @endif
                    </select>
                    @error('trader_fiat_id')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="row mb-3">
                <label class="col-sm-12 col-form-label">{{ __('Payment System') }}</label>
                <div class="col-sm-12">
                    <select name="payment_system_id" class="form-control @error('payment_system_id') is-invalid @enderror" id="payment_system_select">

                    </select>
                    @error('payment_system_id')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="row mb-3">
                <label class="col-sm-12 col-form-label">{{ __('Payment System Type') }}</label>
                <div class="col-sm-12">
                    <select name="payment_system_type_id" class="form-control" id="payment_type_select">
                    </select>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="row mb-3">
                <label class="col-sm-12 col-form-label" id="card_number">{{__('Payment Account')}}</label>
                <div class="col-sm-12">
                    <input type="text" name="card_number" class="form-control @error('card_number') is-invalid @enderror" value="{{ old('card_number') }}">
                    @error('card_number')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row mb-3">
                <label class="col-sm-12 col-form-label" >{{__('Limit Amount')}}</label>
                <div class="col-sm-12">
                    <input type="text" name="limit_amount" class="form-control @error('limit_amount') is-invalid @enderror" value="{{ old('limit_amount')}}">
                    @error('limit_amount')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="col-lg-6" id="payment_info" @if(old('payment_info') == null) style="display:none" @endif>
            <div class="row mb-3">
                <label class="col-sm-12 col-form-label">{{ __('Payment Info') }}</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="payment_info" value="{{old('payment_info')}}">
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <label class="col-sm-12 col-form-label">&nbsp;</label>
            <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
        </div>
    </form>
@endsection

@section('script-bottom')
<script>
    const csrf = document
        .querySelector('meta[name="csrf-token"]')
        .getAttribute('content')
    $(function(){
        $('#trader_fiat_id').trigger('change');
    })
    $(document).on('change', '#trader_fiat_id', function(){
        $('#payment_system_select').html('');
        $('#payment_type_select').html('');
        var trader_fiat_data = {
            id: $(this).val(),
            _token: csrf
        };
        $.ajax({
            url:'{{ route('trader.payment-system-accounts.getpaysystems') }}',
            data: trader_fiat_data,
            type:"POST",
            success: function(response){
                var options = '';
                response.forEach(function(r){
                    var old_id = '{{ old('trader_fiat_id') }}';
                    var selected = '';
                    if(old_id && old_id == r.id){
                        selected = 'selected';
                    }
                    options += '<option ' + selected + ' value="' + r.id + '">' + (tt[r.name] || r.name) + '</option>';

                });
                $('#payment_system_select').html(options);
                $(document).on('change', '#payment_system_select', function(){
                    $('#payment_info').hide();

                    var data = {
                        id: $(this).val(),
                        _token: csrf
                    };
                    $.ajax({
                        url:'{{ route('trader.payment-system-accounts.gettypes') }}',
                        data:data,
                        type:"POST",
                        success: function(response){
                            var options = '',
                                iban;
                            response.forEach(function(r){
                                if(r.obj_name && r.obj_name == 'iban'){
                                    iban = r.id;
                                }
                                var old_id = '{{old('payment_system_type_id')}}';
                                var selected = '';
                                if(old_id == r.id){
                                    selected = 'selected';
                                }
                                options += '<option ' + selected + ' value="' + r.id + '">' + (tt[r.name] || r.name) + '</option>';
                            })
                            $('#payment_type_select').html(options);
                            $(document).on('change', '#payment_type_select', function(){
                                if(iban != undefined && $(this).val() == iban){
                                    $('#payment_info').show();
                                } else {
                                    $('#payment_info').hide();
                                }
                            })
                            $('#payment_type_select').trigger('change');
                        }
                    })
                })
                $('#payment_system_select').trigger('change');
            }
        })
    })
</script>
@endsection

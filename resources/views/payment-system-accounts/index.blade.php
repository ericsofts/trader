<?php
use App\Models\TraderPaymentAccount;
?>
@extends('layouts.app')
@section('content')

    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Payment Accounts')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Payment Accounts') }}
        </h1>
        <a href="{{ route('trader.payment-system-accounts.new') }}" class="btn btn-grow">{{__('Add New')}}</a>
    </div>

    @if($accounts != null)
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>{{ __('Name') }}</th>
                        <th>{{ __('Payment System') }}</th>
                        <th>{{ __('Fiat') }}</th>
                        <th>{{__('Payment Account')}}</th>
                        <th>{{__('Payment Info')}}</th>
                        <th>{{__('Limit Amount')}}</th>
                        <th>{{__('Volume')}}</th>
                        <th>{{__('Action')}}</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($accounts as $k => $account)
                    <tr>
                        <td>{{ $account->name }}</td>
                        <td>{{ $account->paysystem->name }}</td>
                        <td>{{ $account->fiat->name }}</td>
                        <td>{{ $account->card_number }}</td>
                        <td>{{ $account->payment_info }}</td>
                        <td>{{ $account->limit_amount }}</td>
                        <td> {{ $account->volume }}</td>
                        <td class="btn-group">
                            @if ($account->status == TraderPaymentAccount::STATUS_ACTIVE)
                                <form action="{{route('trader.payment-system-accounts.disable', ['id' => $account->id])}}" method="post" class="btn btn-outline-secondary p-0">
                                    @csrf
                                    <button class="btn-confirm-default btn btn-link" title="{{__('Disable')}}"><i class="bi bi-stop-circle"></i></button>
                                </form>
                            @else
                                <form action="{{route('trader.payment-system-accounts.enable', ['id' => $account->id])}}" method="post" class="btn btn-outline-secondary p-0">
                                    @csrf
                                    <button class="btn-confirm-default btn btn-link" title="{{__('Enable')}}"><i class="bi bi-play-circle"></i></button>
                                </form>
                            @endif
                            <form action="{{route('trader.payment-system-accounts.edit', ['id' => $account->id])}}" method="get" class="btn btn-outline-secondary p-0">
                                @csrf
                                <button class="btn btn-link" title="{{__('Edit')}}">
                                    <i class="bi bi-pencil-square"></i>
                                </button>
                            </form>
                            <form action="{{route('trader.payment-system-accounts.reset', ['id' => $account->id])}}" method="post" class="btn btn-outline-secondary p-0">
                                @csrf
                                <button class="btn-confirm-default btn btn-link" title="{{__('Reset')}}">
                                    <i class="bi bi-arrow-repeat"></i>
                                </button>
                            </form>
                            <form action="{{route('trader.payment-system-accounts.delete', ['id' => $account->id])}}" method="post" class="btn btn-outline-secondary p-0">
                                @csrf
                                <button class="btn-confirm-default btn btn-link" title="{{__('Delete')}}">
                                    <i class="bi bi-x"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif
@endsection

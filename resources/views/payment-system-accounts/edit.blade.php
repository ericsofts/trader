@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('trader.payment-system-accounts')}}">{{__('Payment Accounts')}}</a></li>
            <li class="breadcrumb-item active">{{__('Edit Payment Account')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Edit Payment Account') }}
        </h1>
    </div>
    <form method="POST" action="{{ route('trader.payment-system-accounts.update', ['id' => $account->id]) }}" class="row mb-3">
        @csrf
        @method('POST')
        <div class="col-lg-3">
            <div class="row mb-3">
                <label class="col-sm-12 col-form-label">{{ __('Name') }}</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="name" value="{{ old('name', $account->name) }}">
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="row mb-3">
                <label class="col-sm-12 col-form-label">{{ __('Fiat') }}</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" value="{{ $account->fiat->name }}" readonly>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="row mb-3">
                <label class="col-sm-12 col-form-label">{{ __('Payment System') }}</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" value="{{ $account->paysystem->name }}" readonly>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row mb-3">
                <label class="col-sm-12 col-form-label">{{ __('Payment System Type') }}</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" value="{{ __($account->payType->name) }}" readonly>
                    <input type="hidden" name="payment_system_type_id" value="{{ $account->payType->id }}">
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row mb-3">
                <label class="col-sm-12 col-form-label" id="card_number">{{__('Payment Account')}}</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control @error('card_number') is-invalid @enderror" name="card_number" value="{{ old('card_number', $account->card_number) }}">
                    @error('card_number')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>
         <div class="col-lg-6">
            <div class="row mb-3">
                <label class="col-sm-12 col-form-label" >{{__('Limit Amount')}}</label>
                <div class="col-sm-12">
                    <input type="text" name="limit_amount" class="form-control @error('limit_amount') is-invalid @enderror"  value="{{ old('limit_amount', $account->limit_amount) }}">
                    @error('limit_amount')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                </div>
            </div>
        </div>
        @if($account->payment_info)
            <div class="col-lg-6">
                <div class="row mb-3">
                    <label class="col-sm-12 col-form-label">{{ __('Payment Info') }}</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="payment_info" value="{{ old('payment_info', $account->payment_info) }}">
                    </div>
                </div>
            </div>
        @endif
        <div class="col-md-12">
            <label class="col-sm-12 col-form-label" id="account_info">&nbsp;</label>
            <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
        </div>
    </form>
@endsection

<?php
    use App\Models\TraderAd;
?>

@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('trader_ad.index', ['type' => $route_types[$type]])}}">{{__('Ads')}}</a></li>
            <li class="breadcrumb-item active">{{__('New')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('New Ad') }}
        </h1>
    </div>
    <form method="POST" action="{{ route('trader_ad.create') }}" class="row mb-3 trader-ad-form">
        @csrf

        <div class="col-md-7">
            <div class="card p-3 mb-3">
                <div class="row">
                    <input type="hidden" value="{{$type}}" name="type" id="type">

                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-sm-12 col-form-label">{{__('Fiat')}}:</label>
                            <div class="col-sm-12">
                                <select class="form-select @error('fiat_id') is-invalid @enderror"
                                    name="fiat_id" id="fiat_id">
                                    <option value="">{{__('Choose')}}</option>
                                    @foreach ($all_fiats as $unit)
                                        <option data-asset="{{$unit->asset}}" value="{{$unit->id}}" @if(old('fiat_id') == (string)$unit->id) selected @endif>{{$unit->name}}</option>
                                    @endforeach
                                </select>

                                @error('fiat_id')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-sm-12 col-form-label">{{__('Payment System')}}:</label>
                            <div class="col-sm-12">
                                <select class="form-select @error('payment_system_id') is-invalid @enderror"
                                    name="payment_system_id" id="payment_system_id"
                                    value="{{old('payment_system_id')}}">
                                    <option value="">{{__('Choose')}}</option>
                                </select>

                                @error('payment_system_id')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-sm-12 col-form-label">{{__('Currency')}}:</label>
                            <div class="col-sm-12">
                                <select class="form-select @error('currency_id') is-invalid @enderror"
                                    name="currency_id" id="currency_id">
                                    @foreach ($all_currencies as $unit)
                                        <option data-asset="{{$unit->asset}}" value="{{$unit->id}}" @if(old('currency_id') == (string)$unit->id) selected @endif>{{$unit->name}}</option>
                                    @endforeach
                                </select>

                                @error('currency_id')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="row">
                            <label class="col-sm-12 col-form-label">{{__('Payment System Type')}}:</label>
                            <div class="col-sm-12">
                                <select class="form-select @error('payment_system_type_id') is-invalid @enderror"
                                    name="payment_system_type_id" id="payment_system_type_id">
                                    @foreach ($all_payment_system_types as $unit)
                                        <option value="{{$unit->id}}"
                                            data-pst="{{$unit->obj_name}}"
                                            @if(old('payment_system_type_id') == $unit->id) selected @endif>{{__($unit->name)}}</option>
                                    @endforeach
                                </select>

                                @error('payment_system_type_id')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="row">
                            <label class="col-sm-12 col-form-label">{{__('Account order')}}:</label>
                            <div class="col-sm-12">
                                <select class="form-select @error('account_order') is-invalid @enderror" name="account_order" id="account_order">
                                    <option value="{{ TraderAd::ACCOUNT_ORDER_RANDOM }}" @if(old('account_order') === (string) TraderAd::ACCOUNT_ORDER_RANDOM) selected @endif>{{__('Random')}}</option>
                                    <option value="{{ TraderAd::ACCOUNT_ORDER_CICLICALLY }}" @if(old('account_order') == (string) TraderAd::ACCOUNT_ORDER_CICLICALLY) selected @endif>{{__('Cyclically')}}</option>
                                </select>

                                @error('account_order')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 type-sell">
                        <div class="row" id="card_number_toggler">
                            <label class="col-sm-12 col-form-label" data-pst-id="{{$unit->id}}">{{__('Payment Accounts')}}:</label>

                            <div class="col-sm-12">
                                <div id="card_number"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="row">
                            <label class="col-sm-12 col-form-label">{{__('Auto-acceptance')}}:</label>
                            <div class="col-sm-12">
                                <select class="form-select @error('auto_acceptance') is-invalid @enderror"
                                        name="auto_acceptance" id="auto_acceptance">
                                    <option value="0" @if(old('auto_acceptance') == 0) selected @endif>{{__('InActive')}}</option>
                                    <option value="1" @if(old('auto_acceptance') == 1) selected @endif>{{__('Active')}}</option>
                                </select>

                                @error('auto_acceptance')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-sm-12 col-form-label">{{__('Min Amount')}}:</label>
                            <div class="col-sm-12">
                                <input type="number" class="form-control @error('min_amount') is-invalid @enderror" step="0.01"
                                    value="{{old('min_amount')}}"
                                    name="min_amount" id="min_amount">

                                @error('min_amount')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-sm-12 col-form-label">{{__('Max Amount')}}:</label>
                            <div class="col-sm-12">
                                <input type="number" class="form-control @error('max_amount') is-invalid @enderror" step="0.01"
                                    value="{{old('max_amount')}}"
                                    name="max_amount" id="max_amount">

                                @error('max_amount')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-7">
            <div class="card p-3 mb-3">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-sm-12 col-form-label">{{__('Rate Type')}}:</label>
                            <div class="col-sm-12">
                                <select class="form-select @error('rate_type') is-invalid @enderror"
                                    name="rate_type" id="rate_type">
                                    <option value="">{{__('Choose')}}</option>
                                    <option value="{{ TraderAd::RATE_FIXED }}" @if(old('rate_type') === (string)TraderAd::RATE_FIXED) selected @endif>{{__('Fixed')}}</option>
                                    <option value="{{ TraderAd::RATE_PERCENTAGE }}" @if(old('rate_type') === (string)TraderAd::RATE_PERCENTAGE) selected @endif>{{__('Percentage')}}</option>
                                </select>

                                @error('rate_type')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-sm-12 col-form-label type-fixed">{{__('Rate')}}:</label>
                            <label class="col-sm-12 col-form-label type-percentage">{{__('Percent')}}:</label>
                            <div class="col-sm-12">
                                <input type="number" class="form-control @error('rate') is-invalid @enderror" step="0.01"
                                    value="{{old('rate')}}"
                                    name="rate" id="rate">

                                @error('rate')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    @if (count($all_rate_sources) > 1)
                    <div class="col-lg-6" id="rate_source_id_toggler">
                        <div class="row">
                            <label class="col-sm-12 col-form-label">{{__('Rate Source')}}:</label>
                            <div class="col-sm-12">
                                <select class="form-select @error('rate_source_id') is-invalid @enderror"
                                    name="rate_source_id" id="rate_source_id">
                                    @foreach ($all_rate_sources as $unit)
                                        <option value="{{$unit->id}}" @if(old('rate_source_id') === $unit->id) selected @endif>{{$unit->name}}</option>
                                    @endforeach
                                </select>

                                @error('rate_source_id')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    @else
                        <input type="hidden" value="1" name="rate_source_id" id="rate_source_id">
                    @endif

                    <div class="col-lg-6" id="temp_rate_toggler">
                        <div class="form-text" id="rate_from_source" style="margin-top: 30px"></div>
                        <div class="form-text">{{__('The price of your ad will be')}} <span id="rate_dynamic"></span></div>
                    </div>

                    <div class="col-lg-6" id="rate_stop_toggler">
                        <div class="row">
                            <label class="col-sm-12 col-form-label type-buy" style="display: none;">{{__('Maximal fixed price in')}} <span class="fiat-asset">{{__('Fiat')}}</span></label>
                            <label class="col-sm-12 col-form-label type-sell" style="display: none;">{{__('Minimal fixed price in')}} <span class="fiat-asset">{{__('Fiat')}}</span></label>
                            <div class="col-sm-12">
                                <input type="number" class="form-control @error('rate_stop') is-invalid @enderror" step="0.01"
                                    value="{{ price_format(old('rate_stop'), 2) }}"
                                    name="rate_stop" id="rate_stop">

                                <div class="form-text type-buy" style="display: none;">{{__('If the reference exchange rate raise, then your price will not raise above this value')}}</div>
                                <div class="form-text type-sell" style="display: none;">{{__('If the reference exchange rate falls, then your price will not fall below this value')}}</div>

                                @error('rate_stop')
                                <div class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-2 col-md-3">
                    <select class="form-select" name="status">
                        <option value="1" @if(old('status') === (string)TraderAd::STATUS_ACTIVE) selected @endif>{{__('Active')}}</option>
                        <option value="0" @if(old('status') === (string)'0') selected @endif>{{__('InActive')}}</option>
                    </select>
                </div>

                <div class="col">
                    <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('script-bottom')
<script>
    var fiats = {!! json_encode($all_fiats) !!};
    var currencies = {!! json_encode($all_currencies) !!};

    var TYPE_BUY = {{TraderAd::TYPE_BUY}};
    var TYPE_SELL = {{TraderAd::TYPE_SELL}};

    var RATE_PERCENTAGE = {{TraderAd::RATE_PERCENTAGE}};
    var RATE_FIXED = {{TraderAd::RATE_FIXED}};

    var RATES_URL = '{{route('trader_ad.rates')}}';
    var AD_ID = 0;
</script>
@endsection

<?php
    use App\Models\TraderAd;
    use App\Models\TraderPaymentAccount;
?>

@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Ads')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Ads') }}
        </h1>
    </div>

    <form method="GET" action="{{ route('trader_ad.index', ['type' => $type]) }}" class="row g-3 mb-3">
        <div class="col-md-3">
            <div class="row">
                <label class="col-sm-2 col-form-label">{{__('Status')}}:</label>
                <div class="col-sm-10">
                    <select class="form-select" name="status">
                        <option value="">{{__('Choose')}}</option>
                        <option value="{{ TraderAd::STATUS_ACTIVE }}" @if($status == TraderAd::STATUS_ACTIVE) selected @endif>{{__('Active')}}</option>
                        <option value="0" @if($status == "0") selected @endif>{{__('InActive')}}</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="row">
                <label class="col-sm-2 col-form-label">{{__('Type')}}:</label>
                <div class="col-sm-10">
                    <select class="form-select" name="type">
                        <option value="">{{__('Choose')}}</option>
                        <option value="{{ TraderAd::TYPE_BUY }}" @if($type == TraderAd::TYPE_BUY) selected @endif>{{__('Buy')}}</option>
                        <option value="{{ TraderAd::TYPE_SELL }}" @if($type == TraderAd::TYPE_SELL) selected @endif>{{__('Sell')}}</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
        </div>

        <div class="col-md-3">
            <a class="btn btn-grow" href="{{ route('trader_ad.new', ['type' => $route_types[$type]]) }}" role="button">{{__('Add New')}}</a>
        </div>
    </form>

    <div class="d-flex mb-3">
        <form action="{{route('trader.ad.enableall')}}" method="post" class="me-3">
            @csrf
            <button class="btn btn-grow">{{__('Enable All')}}</button>
        </form>
        <form action="{{route('trader.ad.disableall')}}" method="post">
            @csrf
            <button class="btn btn-grow">{{__('Disable All')}}</button>
        </form>
    </div>

    <div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">{{__('ID')}}</th>
                <th scope="col">{{__('Type')}}</th>
                <th scope="col">{{__('Currency')}}</th>
                <th scope="col">{{__('Fiat')}}</th>
                <th scope="col">{{__('Payment System')}}</th>
                @if ($type == TraderAd::TYPE_SELL)
                <th scope="col">{{__('Payment Accounts')}}</th>
                @endif
                <th scope="col">{{__('Min Amount')}}</th>
                <th scope="col">{{__('Max Amount')}}</th>
                <th scope="col">{{__('Status')}}</th>
                <th scope="col">{{__('Action')}}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($trader_ads as $i => $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td class="type-{{$item->type}}">{{__($types[$item->type])}}</td>
                    <td>{{$item->currency->name}}</td>
                    <td>{{$item->fiat->name}}</td>
                    <td>
                        <div class="badge badge-ps" style="
                            @if ($item->payment_system->color)
                                color: {{$item->payment_system->color}};
                            @endif
                            @if ($item->payment_system->bg_color)
                                background-color: {{$item->payment_system->bg_color}};
                            @endif
                            ">{{$item->payment_system->name}}</div>
                    </td>
                    @if ($type == TraderAd::TYPE_SELL)
                    <td>
                        @foreach ($item->payment_accounts as $account)
                            <div class="d-flex align-items-center">
                                <form action="{{route('trader.payment-system-accounts.disable', ['id' => $account->id])}}" method="post">
                                    @csrf
                                    <button class="btn-confirm-default btn btn-link me-1 p-0" title="{{__('Disable')}}"><i class="bi bi-stop-circle"></i></button>
                                </form>
                                <span>{{ $account->name ?: $account->card_number }}</span>
                                <span>&nbsp;&mdash;&nbsp;</span>
                                <span>{{ $account->volume ?: 0 }} {{ $item->fiat->asset }}</span>
                                <form action="{{route('trader.payment-system-accounts.reset', ['id' => $account->id])}}" method="post">
                                    @csrf
                                    <button class="btn-confirm-default btn btn-link ms-1 p-0" title="{{__('Reset')}}"><i class="bi bi-arrow-repeat"></i></button>
                                </form>
                            </div>
                        @endforeach
                    </td>
                    @endif
                    <td>{{price_format($item->min_amount, 2)}}</td>
                    <td>{{price_format($item->max_amount, 2)}}</td>
                    <td>{{__(['InActive', 'Active'][$item->status])}}</td>
                    <td>
                        <div class="btn-group mb-3">
                            <a href="{{route('trader_ad.edit', $item->id)}}" class="btn btn-outline-secondary">
                                <i class="bi bi-pencil-square" title="{{__('Edit')}}"></i>
                            </a>
                            @if ($item->status == TraderAd::STATUS_ACTIVE)
                            <form action="{{route('trader.ad.disable', ['id' => $item->id])}}" method="post" class="btn btn-outline-secondary">
                                @csrf
                                <button class="btn btn-link p-0"><i class="bi bi-stop-circle" title="{{__('Disable')}}"></i></button>
                            </form>
                            @else
                            <form action="{{route('trader.ad.enable', ['id' => $item->id])}}" method="post" class="btn btn-outline-secondary">
                                @csrf
                                <button class="btn btn-link p-0"><i class="bi bi-play-circle" title="{{__('Enable')}}"></i></button>
                            </form>
                            @endif
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    </div>
    <div class="float-end">{{ $trader_ads->links() }}</div>
@endsection
